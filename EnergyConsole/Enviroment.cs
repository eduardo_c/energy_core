﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
    /// <summary>
    /// This class must manipulate all the object that emit events and reasigned to other objects
    /// IMPORTANT: Water pits emit an event of empty resource, if trucks are waiting for water then
    /// must quit all an reasigned to other water pit(check the status = Empty).
    /// </summary>
    class Enviroment : Choreographer
    {
        private double _master_timer;

        private List<Route> _routes;  //Do not generate events
        private List<WaterPit> _water_pits;  //Generate events
        private List<WaterTreatmentPlant> _plants_water; //Generate events

        private Dictionary<TruckPipe, IPositionIdTag> _vehicles; //Do not generate events

        private List<RouteTransition> _transition_routes; //Generate events 
        private List<ExtractionZone> _extraction_zones;  //Generate Events

        public Enviroment()
        {
            _routes = new List<Route>();
            _vehicles = new Dictionary<TruckPipe, IPositionIdTag>();
            _water_pits = new List<WaterPit>();
            _plants_water = new List<WaterTreatmentPlant>();
            _transition_routes = new List<RouteTransition>();
            _extraction_zones = new List<ExtractionZone>();
        }

        public bool CheckIfStaticObjectExist(IPositionIdTag e_object, out int indx)
        {
            if(e_object.ObjectTag.Equals(MasterIdTag.ExtractionZoneTag))
            {
                for(int i = 0; i < _extraction_zones.Count; i++)
                {
                    if (e_object == _extraction_zones[i])
                    {
                        indx = i;
                        return true;
                    }
                }
            }
            else if(e_object.ObjectTag.Equals(MasterIdTag.WaterPitTag))
            {
                for (int i = 0; i < _water_pits.Count; i++)
                {
                    if (e_object == _water_pits[i])
                    {
                        indx = i;
                        return true;
                    }
                }
            }
            else if (e_object.ObjectTag.Equals(MasterIdTag.TreatmentPlantTag))
            {
                for (int i = 0; i < _plants_water.Count; i++)
                {
                    if (e_object == _plants_water[i])
                    {
                        indx = i;
                        return true;
                    }
                }
            }
            else
            {
                indx = 0;
                return false;
            }

            indx = 0;
            return false;
        }

        //Push a vheicle but doesn not do nothing, until you assing a route transition
        public void PushNewVehicle(TruckPipe truck, IPositionIdTag asociated_object)
        {
            if (asociated_object.ObjectTag.Equals(MasterIdTag.ExtractionZoneTag))
            {

            }
        }

        public void AddARoute(Route route)
        {
            _routes.Add(route);
        }

        public void AddWaterPit(WaterPit water_pit)
        {
            _water_pits.Add(water_pit);
        }

        public void AddExtractionZone(ExtractionZone extraction_zone)
        {
            _extraction_zones.Add(extraction_zone);
        }

        public Route GetRouteById(int id)
        {
            for(int i = 0; i < _routes.Count; i++)
            {
                if(_routes[i].ObjectId == id)
                {
                    return _routes[i];
                }
            }
            return null;
        }

        //Chek if there are transitions with this route?? And if have vehicles
        public Route DeleteRouteById(int id)
        {
            return null;
        }

        //route must be in the routes list, if not, append it
        public void CreateNewRouteTransition(Route route)
        {
            if (GetRouteById(route.ObjectId) != null)
            {
                _transition_routes.Add(new RouteTransition(this, route));
            }
            else
            {
                _routes.Add(route);
                _transition_routes.Add(new RouteTransition(this, route));
            }
        }

        //Not sure yet
        public void Update()
        {
            
        }

        public void GetNextEvent()
        {

        }

        public double MasterTimeCounter
        {
            get
            {
                return _master_timer;
            }
            set
            {
                _master_timer = value;
            }
        }
    }
}
