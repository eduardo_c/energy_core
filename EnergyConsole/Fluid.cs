﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;

namespace Energy
{ 
    /// <summary>
    /// Class for represent fluids
    /// </summary>
    class Fluid
    {
        /// <summary>
        /// Enumeration for fluid tags
        /// </summary>
        public enum FluidTags : byte { FreshWater, DirtyWater, Unknown };
        
        #region atributes
        /// <summary>
        /// Dinamic viscosity
        /// </summary>
        protected double _viscosity_dynamic;  //in poiseuilles
        /// <summary>
        /// Kinematic viscosity
        /// </summary>
        protected double _viscosity_kinematic;
        /// <summary>
        /// Density
        /// </summary>
        protected double _density; //in grams/cubic meters
        /// <summary>
        /// Tag of the fluid or name
        /// </summary>
        private FluidTags _tag;
        #endregion

        #region methods
        /// <summary>
        /// Constuctor
        /// </summary>
        /// <param name="viscosity_dynamic">Dinamic viscosity</param>
        /// <param name="density">Density</param>
        public Fluid(double viscosity_dynamic, double density)
        {
            this._viscosity_dynamic = viscosity_dynamic;
            this._viscosity_kinematic = KinematicViscosities.FreshWater23deg;
            this._density = density;
            this._tag = FluidTags.Unknown;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Fluid()
        {
            this._viscosity_dynamic = DynamicViscosities.FreshWater20deg;
            this._viscosity_kinematic = KinematicViscosities.FreshWater20deg;
            this._density = Densities.Water;
            this._tag = FluidTags.FreshWater;

        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tag">Tag of the fluid</param>
        public Fluid(FluidTags tag)
        {
            this._viscosity_dynamic = DynamicViscosities.FreshWater20deg;
            this._viscosity_kinematic = KinematicViscosities.FreshWater20deg;
            this._density = Densities.Water;
            this._tag = tag;
        }

        /// <summary>
        /// Creates a new intance of fluid with same atributes
        /// </summary>
        /// <returns>A clone of te actual instance</returns>
        public Fluid Clone()
        {
            return (Fluid)MemberwiseClone();
        }
        /// <summary>
        /// Property for get/set the dinamic viscosity
        /// </summary>
        public double ViscosityDynamic
        {
            get { return _viscosity_dynamic; }
            set { this._viscosity_dynamic = value; }
        }
        /// <summary>
        /// Property for get/set the kinematic viscosity
        /// </summary>
        public double ViscosityKinematic
        {
            get { return _viscosity_kinematic; }
            set { _viscosity_kinematic = value; }
        }
        /// <summary>
        /// Property for get/set the density
        /// </summary>
        public double Density
        {
            get { return _density; }
            set { _density = value; }
        }
        /// <summary>
        /// Property for get/set the fluid tag
        /// </summary>
        public FluidTags Tag
        {
            get { return this._tag; }
            set { _tag = value; }
        }

        #endregion

        #region subclass_container
        /// <summary>
        /// Subclass of fluid for represent a fluid container
        /// </summary>
        public class Container : ICost
        {
            //System.Object obj = new System.Object();
            /// <summary>
            /// Enumeration for know the container status
            /// </summary>
            public enum Status : byte { Filling, Draining, Stable, Fill, Drain }
            /// <summary>
            /// Maximum capacity for the container
            /// </summary>
            protected Measure.Volume _capacity;
            /// <summary>
            /// Tipe of fluid that carry
            /// </summary>
            protected Fluid _fluid;
            /// <summary>
            /// Actual cantity of fluid in the container
            /// </summary>
            protected Measure.Volume _actual_quantity;
            /// <summary>
            /// Input flow for fill the container
            /// </summary>
            protected Measure.FlowRate _iflow;
            /// <summary>
            /// Output flow for drain the container
            /// </summary>
            protected Measure.FlowRate _oflow;
            /// <summary>
            /// Stores the actual stuatus of the container
            /// </summary>
            protected Status _status;

            public double Cost { get; set; }

            public CostsTypes CostType { get; set; }

            public double GetTotalCost()
            {
                return Cost;
            }

            /// <summary>
            /// Creates a new instance of container with the same speceifications
            /// </summary>
            /// <returns>A clone of the actual container</returns>
            public Container Clone()
            {
                Container c = (Container)MemberwiseClone();
                c.FluidType = FluidType.Clone();
                return c;
            }
            /// <summary>
            /// Constructor
            /// </summary>
            public Container()
            {
                 _capacity = new Measure.Volume(0);
                 _fluid = new Fluid();
                 _actual_quantity = new Measure.Volume(0);
                 _iflow = new Measure.FlowRate(0);
                 _oflow = new Measure.FlowRate(0);
            }
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="capacity">Maximum capacity of the container</param>
            /// <param name="actual_quantity">Actual quatity of the container</param>
            public Container(Measure.Volume capacity, Measure.Volume actual_quantity)
            {

                _capacity = capacity;
                _actual_quantity = actual_quantity;
                
                _fluid = new Fluid();
                _iflow = new Measure.FlowRate(0);
                _oflow = new Measure.FlowRate(0);
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="capacity"></param>
            /// <param name="actual_quantity"></param>
            public Container(Measure.Volume capacity, Measure.Volume actual_quantity, FluidTags type)
            {

                _capacity = capacity;
                _actual_quantity = actual_quantity;

                _fluid = new Fluid(type);
                _iflow = new Measure.FlowRate(0);
                _oflow = new Measure.FlowRate(0);
            }

            /// <summary>
            /// Property for get/set de capacity of the container
            /// </summary>
            public Measure.Volume Capacity
            {
                get { return _capacity; }
                set { _capacity = value; }
            }

            /// <summary>
            /// Property for get set the kind of fluid that carries
            /// </summary>
            public Fluid FluidType
            {
                get { return _fluid; }
                set { _fluid = value; }
            }
            /// <summary>
            /// Property for get/set the actual quantity
            /// </summary>
            public Measure.Volume ActualQuantity
            {
                get { return _actual_quantity; }
                set
                {
                    if (value > _capacity)
                    {
                        _actual_quantity = _capacity;
                    }
                    else
                    {
                        if (value.Value < 0) _actual_quantity = Measure.Volume.ZERO;
                        else _actual_quantity = value;
                    }
                }
            }
            /// <summary>
            /// Property for get/set the input flow for the container
            /// </summary>
            public Measure.FlowRate IFlow
            {
                set { _iflow = value; }
                get { return _iflow; }
            }
            /// <summary>
            /// Property for get/set the output flow for the container
            /// </summary>
            public Measure.FlowRate OFlow
            {
                set { _oflow = value; }
                get { return _oflow; }
            }
            /// <summary>
            /// Property for get the fluid status
            /// </summary>
            public Status FlowStatus
            {
                get
                {
                    if (_iflow > _oflow)
                    {
                        _status = Status.Filling;
                    }
                    else
                    {
                        if (_iflow < _oflow)
                        {
                            _status = Status.Draining;
                        }
                        else
                        {
                            _status = Status.Stable;
                        }
                    }
                    return _status;
                }
                //set { _status = value; }
            }
            /// <summary>
            /// Sets the flows of the container
            /// </summary>
            /// <param name="iflow">Input flow or filling flow</param>
            /// <param name="oflow">Output flow or draining flow</param>
            public void SetFlows(Measure.FlowRate iflow, Measure.FlowRate oflow)
            {
                _iflow = iflow;
                _oflow = oflow;
            }
            /// <summary>
            /// Checks if the container is actually filled
            /// </summary>
            /// <returns></returns>
            public bool IsFilled()
            {
                return _actual_quantity.Equals(_capacity);
            }
            /// <summary>
            /// Checks if the container is actually empty
            /// </summary>
            /// <returns></returns>
            public bool IsEmpty()
            {
                return _actual_quantity.Value == 0;
            }
            /// <summary>
            /// Force to fill the container
            /// </summary>
            public void Fill()
            {
                _actual_quantity = _capacity;
            }
            /// <summary>
            /// Force to empty the container
            /// </summary>
            public void Empty()
            {
                _actual_quantity.Value = 0;
            }
            /// <summary>
            /// Given the event calculates the time for the ocurrence
            /// </summary>
            /// <param name="ev">Input event for when it happen</param>
            /// <param name="percent">An optional value for restrinct the the maximum capacity of the container for a certain percent</param>
            /// <returns>Time for the event, it's infinite if the event never occur</returns>
            public Measure.Time TimeToEvent(Status ev, double percent = 1.0)
            {
                if (ev == Status.Stable)
                {
                    return Measure.Time.INFINITE;
                }
                else if(ev == Status.Drain)
                {
                    return TimeToEmptyPerFlow(percent);
                }
                else if(ev == Status.Fill)
                {
                    return TimeToFillPerFlow(percent);
                }
                else
                {
                    return Measure.Time.INFINITE;
                }
            }

            /// <summary>
            /// Calculates the needed time to fill the container, based on its actual flows
            /// </summary>
            /// <param name="percent">Optional variable to restrinct the maximum capacity to a certain percent</param>
            /// <returns>Time to fill the container, infinite in never happen</returns>
            public Measure.Time TimeToFillPerFlow(double percent = 1.0)
            {
                if (_oflow >= _iflow)
                {
                    return Measure.Time.INFINITE;
                }
                else
                {
                    Measure.Volume temp = new Measure.Volume(_capacity.Value * percent, _capacity.Type);
                    if ((temp - _actual_quantity).Value <= 0) return Measure.Time.ZERO;
                    else return (temp - _actual_quantity) / (_iflow - _oflow);
                }
            }
            /// <summary>
            /// Calculates the needed time to fill the container, this ignores the actual configurations
            /// and use the parameter of flow 
            /// </summary>
            /// <param name="percent">Optional variable to restrinct the maximum capacity to a certain percent</param>
            /// <param name="f">Flow rate for filling</param>
            /// <returns>Time to fill the container, infinite if never happen</returns>
            public Measure.Time TimeToFillPerFlow(Measure.FlowRate f, double percent = 1.0)
            {
                Measure.Volume temp = new Measure.Volume(_capacity.Value * percent, _capacity.Type);
                if ((temp - _actual_quantity).Value <= 0) return Measure.Time.ZERO;
                else return (temp - _actual_quantity) / f;
            }
            /// <summary>
            /// Calculates the needed time to empty the container, this ignores the actual configurations
            /// and use the parameter of flow 
            /// </summary>
            /// <param name="percent">Optional variable to restrinct the maximum capacity to a certain percent, muste be in (0, 1)</param>
            /// <param name="f">Flow rate for draining</param>
            /// <returns>Time to empty the container, infinite if never happen</returns>
            public Measure.Time TimeToEmptyPerFlow(Measure.FlowRate f, double inv_percent = 1.0)
            {
                Measure.Volume temp = new Measure.Volume(_capacity.Value * (1.0 - inv_percent), _capacity.Type);
                if ((_actual_quantity - temp).Value <= 0) return Measure.Time.ZERO;
                else return (_actual_quantity - temp) / f;
            }
            /// <summary>
            /// Calculates the time to empty the container based on the flows int atributes
            /// </summary>
            /// <param name="inv_percent">Optional paramenter for restrinct the capacity of the container to
            /// a percent in this case it's inverted the real percent is 1 - inv_percent</param>
            /// <returns></returns>
            public Measure.Time TimeToEmptyPerFlow(double inv_percent = 1.0)
            {
                if (_oflow <= _iflow)
                {
                    return Measure.Time.INFINITE;
                }
                else
                {
                    double real_percent = 1.0 - inv_percent;
                    Measure.Volume temp = new Measure.Volume(_capacity.Value * real_percent, _capacity.Type);
                    if ((_actual_quantity - temp).Value <= 0) return Measure.Time.ZERO;
                    return (_actual_quantity - temp) / (_oflow - _iflow);
                }
            }

            /// <summary>
            /// Assuming that the container is filled, but without make changes, calculates the
            /// of emptied
            /// </summary>
            /// <param name="f">Flow rate for drain</param>
            /// <param name="inv_percent">Inverse percent for restrinct the container capacity</param>
            /// <returns>Time for empty the container, check for infinite and zero time</returns>
            public Measure.Time MaximumTimeToEmpty(Measure.FlowRate f, double inv_percent = 1.0)
            {
                if (f.Value <= 0) return Measure.Time.INFINITE;
                double real_percent = 1.0 - inv_percent;
                Measure.Volume temp = new Measure.Volume(_capacity.Value * real_percent, _capacity.Type);
                if ((_capacity - temp).Value <= 0)
                    return Measure.Time.ZERO;
                else
                    return (_capacity - temp) / f;
            }

            /// <summary>
            /// Assuming that the container is empty, without make changes, calculates the time to fill
            /// </summary>
            /// <param name="f">Flow rate for fill the container</param>
            /// <param name="percent">Percent to restrinct the container to a certain percent</param>
            /// <returns>Time to fill the container, check for infinite or zero time</returns>
            public Measure.Time MaximumTimeToFill(Measure.FlowRate f, double percent = 1)
            {
                if (f.Value <= 0) return Measure.Time.INFINITE;
                Measure.Volume temp = new Measure.Volume(_capacity.Value * percent, _capacity.Type);
                if (temp.Value <= 0)
                    return Measure.Time.ZERO;
                else
                    return temp / f;
            }
            /// <summary>
            /// Based on the flows in the atributes checks what event is gona occur
            /// </summary>
            /// <returns>The next event to occur</returns>
            public Status NextEventBasedOnFlows()
            {
                Status st;
                if (_iflow > _oflow)
                {
                    st = Status.Fill;
                }
                else
                {
                    if (_iflow < _oflow)
                    {
                        st = Status.Drain;
                    }
                    else
                    {
                        st = Status.Stable;
                    }
                }
                return st;
            }

            public static Container BuildFromJson(JObject json_obj)
            {
                JToken cap = json_obj["capacity"];
                JToken act = json_obj["actual_quantity"];
                JToken type = json_obj["fluid_type"];
                if(cap != null && act != null)
                {
                    Container cont = new Container(new Measure.Volume((double)cap, Measure.Volume.MetricType.Liter), new Measure.Volume((double)act, Measure.Volume.MetricType.Liter));
                    if(type != null)
                    {
                        string ftype = (string)type;
                        if (ftype.Equals("FRESH_WATER") || ftype.Equals("CLEAN_WATER") || ftype.Equals("CleanWater") || ftype.Equals("FreshWater"))
                            cont.FluidType.Tag = FluidTags.FreshWater;
                        else
                            cont.FluidType.Tag = FluidTags.DirtyWater;
                    }
                    return cont;
                }
                return null;
            }
        }

        #endregion

        public class Pump : ICost
        {
            public double Cost { get; set; }
            public CostsTypes CostType { get; set; }
            
            public double GetTotalCost()
            {
                return Cost;
            }

        }

        #region subclass_dynvisc
        //Poiseuille
        public static class DynamicViscosities
        {
            public static double FreshWater2deg = 1.6735;
            public static double FreshWater3deg = 1.619;
            public static double FreshWater4deg = 1.5673;
            public static double FreshWater5deg = 1.5182;
            public static double FreshWater6deg = 1.4715;
            public static double FreshWater7deg = 1.4271;
            public static double FreshWater8deg = 1.3847;
            public static double FreshWater9deg = 1.3444;
            public static double FreshWater10deg = 1.3059;
            public static double FreshWater11deg = 1.2692;
            public static double FreshWater12deg = 1.234;
            public static double FreshWater13deg = 1.2005;
            public static double FreshWater14deg = 1.1683;
            public static double FreshWater15deg = 1.1375;
            public static double FreshWater16deg = 1.1081;
            public static double FreshWater17deg = 1.0798;
            public static double FreshWater18deg = 1.0526;
            public static double FreshWater19deg = 1.0266;
            public static double FreshWater20deg = 1.0016;
            public static double FreshWater21deg = 0.9775;
            public static double FreshWater22deg = 0.9544;
            public static double FreshWater23deg = 0.9321;
            public static double FreshWater24deg = 0.9107;
            public static double FreshWater25deg = 0.89;
            public static double FreshWater26deg = 0.8701;
            public static double FreshWater27deg = 0.8509;
            public static double FreshWater28deg = 0.8324;
            public static double FreshWater29deg = 0.8145;
            public static double FreshWater30deg = 0.7972;
            public static double FreshWater31deg = 0.7805;
            public static double FreshWater32deg = 0.7644;
            public static double FreshWater33deg = 0.7488;
            public static double FreshWater34deg = 0.7337;
            public static double FreshWater35deg = 0.7191;
            public static double FreshWater36deg = 0.705;
            public static double FreshWater37deg = 0.6913;
            public static double FreshWater38deg = 0.678;
            public static double FreshWater39deg = 0.6652;
            public static double FreshWater40deg = 0.6527;
            public static double FreshWater45deg = 0.5958;
            public static double FreshWater50deg = 0.5465;
            public static double FreshWater55deg = 0.5036;
            public static double FreshWater60deg = 0.466;
            public static double FreshWater65deg = 0.4329;
            public static double FreshWater70deg = 0.4035;
            public static double FreshWater75deg = 0.3774;
            public static double FreshWater80deg = 0.354;
        }

        #endregion

        #region subclass_kinvis
        //Poiseuille
        public static class KinematicViscosities
        {
            public static double FreshWater2deg = 1.6736;
            public static double FreshWater3deg = 1.6191;
            public static double FreshWater4deg = 1.5674;
            public static double FreshWater5deg = 1.5182;
            public static double FreshWater6deg = 1.4716;
            public static double FreshWater7deg = 1.4272;
            public static double FreshWater8deg = 1.3849;
            public static double FreshWater9deg = 1.3447;
            public static double FreshWater10deg = 1.3063;
            public static double FreshWater11deg = 1.2696;
            public static double FreshWater12deg = 1.2347;
            public static double FreshWater13deg = 1.2012;
            public static double FreshWater14deg = 1.1692;
            public static double FreshWater15deg = 1.1386;
            public static double FreshWater16deg = 1.1092;
            public static double FreshWater17deg = 1.0811;
            public static double FreshWater18deg = 1.0541;
            public static double FreshWater19deg = 1.0282;
            public static double FreshWater20deg = 1.0034;
            public static double FreshWater21deg = 0.9795;
            public static double FreshWater22deg = 0.9565;
            public static double FreshWater23deg = 0.9344;
            public static double FreshWater24deg = 0.9131;
            public static double FreshWater25deg = 0.8926;
            public static double FreshWater26deg = 0.8729;
            public static double FreshWater27deg = 0.8539;
            public static double FreshWater28deg = 0.8355;
            public static double FreshWater29deg = 0.8178;
            public static double FreshWater30deg = 0.8007;
            public static double FreshWater31deg = 0.7842;
            public static double FreshWater32deg = 0.7682;
            public static double FreshWater33deg = 0.7528;
            public static double FreshWater34deg = 0.7379;
            public static double FreshWater35deg = 0.7234;
            public static double FreshWater36deg = 0.7095;
            public static double FreshWater37deg = 0.6959;
            public static double FreshWater38deg = 0.6828;
            public static double FreshWater39deg = 0.6702;
            public static double FreshWater40deg = 0.6579;
            public static double FreshWater45deg = 0.6017;
            public static double FreshWater50deg = 0.5531;
            public static double FreshWater55deg = 0.5109;
            public static double FreshWater60deg = 0.474;
            public static double FreshWater65deg = 0.4415;
            public static double FreshWater70deg = 0.4127;
            public static double FreshWater75deg = 0.3872;
            public static double FreshWater80deg = 0.3643;
            public static double Gasoline = 0.006;
        }
        #endregion

        #region subclass_densities
        //g/cubic meter
        public class Densities
        {
            public static double Water = 1000000;
        }
        #endregion
    }
}
