﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
    //enum CostsTypes : byte { Buy, RentContract1, RentContract2, RentContract3, RentContract6, RentContract12 }
    enum CostsTypes : int { Rent, Buy}
    // enum ContractEvents : byte {Broken, Finished}
    interface ICost
    {
        CostsTypes CostType { get; set; }
        double Cost { get; set; }
        double GetTotalCost();
    }
}
