﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
    class SimpleFluidPipe
    {
        #region atributes
        protected double _diameter;  //in meters
        protected double _pressure_init;  //in pascals
        protected double _pressure_end;  //in pascals
        protected double _lenght;  //in meters
        protected Fluid _fluid;
        #endregion

        #region methods
        public SimpleFluidPipe()
        {
            _diameter = 0.1;
            _pressure_end = _pressure_init = 101325;
            _lenght = 1;
            _fluid = new Fluid(Fluid.DynamicViscosities.FreshWater20deg, Fluid.Densities.Water);
        }

        public SimpleFluidPipe(double diameter, double init_pressure, double end_pressure, double length, Fluid fluid)
        {
            _diameter = diameter;
            _pressure_end = end_pressure;
            _pressure_init = init_pressure;
            _lenght = length;
            _fluid = fluid;
        }

        public double Diameter
        {
            get { return _diameter; }
            set { this._diameter = value; }
        }

        public double InitialPressure
        {
            get { return _pressure_init; }
            set { this._pressure_init = value; }
        }

        public double EndPressure
        {
            get { return _pressure_end; }
            set { this._pressure_end = value; }
        }

        public double Lenght
        {
            get { return _lenght; }
            set { this._lenght = value; }
        }

        public Fluid PipeFluid
        {
            get { return _fluid; }
            set { this._fluid = value; }
        }

        //cubic m / sec
        virtual public double GetFlow()
        {
            return (Math.PI * (_pressure_end - _pressure_init) * (Math.Pow(Diameter / 2, 4))) / (8 * _fluid.ViscosityDynamic * _lenght);
        }
        #endregion
    }


    class HorizontalPipe : SimpleFluidPipe
    {
        #region methods
        public HorizontalPipe() : base()
        {

        }

        public HorizontalPipe(double diameter, double init_pressure, double end_pressure, double length, Fluid fluid) :
            base(diameter, init_pressure, end_pressure, length, fluid)
        {

        }

        public override double GetFlow()
        {
            return base.GetFlow();
        }
        #endregion
    }

    class InclinedPipe : SimpleFluidPipe
    {
        //inclination in degrees
        private double _inclination;

        #region methods
        public InclinedPipe() : base()
        {
            _inclination = 0;
        }

        public InclinedPipe(double diameter, double init_pressure, double end_pressure, double length, Fluid fluid, double inclination) :
            base(diameter, init_pressure, end_pressure, length, fluid)
        {
            _inclination = inclination;
        }
        
        public double Inclination
        {
            get { return _inclination; }
            set { _inclination = value; }
        }

        public override double GetFlow()
        {
            double up = ((_pressure_init - _pressure_end) - _fluid.Density * UnitConversor.Constants.Gravity * _lenght * Math.Sin(_inclination)) * Math.PI * Math.Pow(_diameter, 4);
            double down = 128 * _fluid.ViscosityDynamic * _lenght;
            return up / down;
        }
        #endregion
    }

    /// <summary>
    /// Interface for piping fluid supply
    /// </summary>
    interface IPipeSupply
    {
        /// <summary>
        /// Verify the amount of fluid that can supply in a certain time
        /// </summary>
        /// <param name="delta_t">Time for calculate the volume of fluid</param>
        /// <param name="branch"></param>
        /// <returns></returns>
        Measure.Volume CheckSupplyPerTime(Measure.Time delta_t, int branch = 0);
        //if need to update something in the implementation class
        void Consume(Measure.Volume amount, int brach = 0);
        int GetSupplyBranch(IPositionIdTag asociated_obj);
        Measure.FlowRate GetSupplyFlow(int branch = 0);
        void OpenCloseSupply(bool openclose, int branch = 0);
    }

    //discharge is the inverse of supply
    interface IPipeDischarge
    {
        //how much amount of fluid can I send trough this pipe in certain time?
        Measure.Volume CheckDischargePerTime(Measure.Time delta_t, int branch = 0);
        //if need to update something in the implementation class
        void Discharge(Measure.Volume amount, int branch = 0);
        Measure.FlowRate GetDischargeFlow(int branch = 0);
        int GetDischargeBranch(IPositionIdTag asociated_obj);
        void OpenCloseDischarge(bool openclose, int branch = 0);
    }

    //Just for testing
    class TrivialPipeSystem : IPipeSupply, IPipeDischarge
    {
        private Measure.FlowRate _my_flow;
        public TrivialPipeSystem()
        {
            _my_flow = new Measure.FlowRate(1, Measure.FlowRate.MetricType.LiterPerSec);
        }

        public Measure.Volume CheckSupplyPerTime(Measure.Time delta_t, int branch = 0)
        {
            //check more variables(pipe is active, supply empty, etc)
            if (delta_t.Nequal(Measure.Time.INFINITE))
            {
                return delta_t * _my_flow;
            }
            else
            {
                return Measure.Volume.ZERO;
            }
        }

        public Measure.FlowRate GetSupplyFlow(int branch = 0)
        {
            //check if is open or closed
            return _my_flow;
        }

        public void OpenCloseSupply(bool openclose, int branch = 0)
        {

        }

        public void Consume(Measure.Volume consumtion, int branch = 0)
        {
            //do something
        }

        public Measure.Volume CheckDischargePerTime(Measure.Time delta_t, int branch = 0)
        {
            if (delta_t.Nequal(Measure.Time.INFINITE))
            {
                return delta_t * _my_flow;
            }
            else
            {
                return Measure.Volume.ZERO;
            }
        }

        public void Discharge(Measure.Volume discharge, int branch = 0)
        {
            //do something
        }

        public Measure.FlowRate GetDischargeFlow(int branch = 0)
        {
            return _my_flow;
        }

        public void OpenCloseDischarge(bool openclose, int branch = 0)
        {

        }

        public int GetDischargeBranch(IPositionIdTag obj)
        {
            return 0;
        }

        public int GetSupplyBranch(IPositionIdTag obj)
        {
            return 0;
        }
    }
}
