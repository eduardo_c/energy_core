﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.IO;

namespace Energy
{
    /// <summary>
    /// Class for represent an extraction zone
    /// </summary>
    class ExtractionZone : IPositionIdTag
    {
        private static JSchema _schema = null;
        private static bool _schema_loaded = false;

        /// <summary>
        /// Enumeration of posible status, in the case that DirtyEmpty, CleanFilled and DirtyEmptyCleanFilled
        /// also means that is in waiting mode (some time), so this maybe is not be true but it's used to control
        /// </summary>
        public enum Status { Inactive, Normal, CleanEmpty, DirtyEmpty, CleanFilled, DirtyFilled, BothFilled, BothEmpty, DirtyFilledCleanEmpty, CleanFilledDirtyEmpty }
        /// <summary>
        /// Enumeration of all posible events
        /// </summary>
        public enum Events { TruckDrainedClean, TruckFilledDirty, CleanContainerEmpty, CleanContainerFilled, DirtyContainerEmpty, DirtyContainerFilled, InitialConditionAchieved, CleanWaitFinish, DirtyWaitFinish }
        /// <summary>
        /// ID of the object
        /// </summary>
        private int _id;
        /// <summary>
        /// Tag of all instaces
        /// </summary>
        private string _tag = MasterIdTag.ExtractionZoneTag;
        /// <summary>
        /// Counter for generate unique IDs
        /// </summary>
        private static int _id_counter = 0;
        /// <summary>
        /// Extraction pit on the zone
        /// </summary>
        private ExtractionPit _extraction_pit;
        /// <summary>
        /// Clean water container, stores the incoming water from truck and pipes
        /// </summary>
        private Fluid.Container _clean_water;
        /// <summary>
        /// Disrty water container, stores the output water from de batery
        /// </summary>
        private Fluid.Container _dirty_water;
        /// <summary>
        /// Percent of clean water in container to init the extraction
        /// </summary>
        private double _clean_water_percent_init;
        /// <summary>
        /// List for represent a queue where is atached the incoming trucks who carry fresh water
        /// </summary>
        private List<TruckPipe> _clean_water_queue;
        /// <summary>
        /// List for represent a queue where is stored the incoming trucks whi carry dirty water
        /// </summary>
        private List<TruckPipe> _dirty_water_queue;
        /// <summary>
        /// Maybe not necesary
        /// </summary>
        private Enviroment _context;
        /// <summary>
        /// Flow rate for pass water from truck to the container
        /// </summary>
        private Measure.FlowRate _clean_pump_flow;
        /// <summary>
        /// Flow rate for pass water from the dirty water container to the truck
        /// </summary>
        private Measure.FlowRate _dirty_pump_flow;
        /// <summary>
        /// Volume to store the unreachable demand
        /// </summary>
        private Measure.Volume _missing_water;
        /// <summary>
        /// Variable to store the flloding water from the extracted
        /// </summary>
        private Measure.Volume _flooding;
        /// <summary>
        /// Count the amount of mix(gas) produced by the extraction
        /// </summary>
        private Measure.Volume _mix_produced;
        /// <summary>
        /// Stores the actual state of the extraction zone
        /// </summary>
        private Status _status;
        /// <summary>
        /// In case that the clean container is filled or the dirty container empty
        /// the transference from/to the truck is suspended this time
        /// </summary>
        private Measure.Time _waiting_time;
        /// <summary>
        /// In case that clean container is filled a waiting time is started by count down timer with this time
        /// </summary>
        private Measure.Time _waiting_time_clean;
        /// <summary>
        /// In case that dirty container is emptied a waiting time is started by count down timer with this time
        /// </summary>
        private Measure.Time _waiting_time_dirty;
        /// <summary>
        /// Counting down timer for waiting in clean container
        /// </summary>
        private Measure.Time _waiting_count_clean;
        /// <summary>
        /// Counting down timer for waiting in dirty water container
        /// </summary>
        private Measure.Time _waiting_count_dirty;
        /// <summary>
        /// Interface object for obtain water supply through pipes
        /// </summary>
        private IPipeSupply _pipe_supply_clean;
        /// <summary>
        /// Interface object for obtain dirty water discharge through pipes
        /// </summary>
        private IPipeDischarge _pipe_discharge_dirty;
        /// <summary>
        /// List for store finished vehicles attached actually to this instance
        /// </summary>
        private List<TruckPipe> _finished_cache;

        /// <summary>
        /// Increments the counter id and returns a new unique ID
        /// </summary>
        /// <returns>
        /// Unique ID for the object
        /// </returns>
        private int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">Context, an Enviroment instance for accesing other variables in the context, assign it null, it's not needed</param>
        /// <param name="pit">Extraction pit instance for the extraction zone</param>
        /// <param name="clean_water">Container specifications for the clean water</param>
        /// <param name="dirty_water">Container specifications for the dirty water</param>
        public ExtractionZone(Enviroment context, ExtractionPit pit, Fluid.Container clean_water, Fluid.Container dirty_water)
        {
            _id = IdIncrement();

            _clean_pump_flow = new Measure.FlowRate(3, Measure.FlowRate.MetricType.LiterPerSec);
            _dirty_pump_flow = new Measure.FlowRate(3, Measure.FlowRate.MetricType.LiterPerSec);
            _context = context;
            _extraction_pit = pit;
            _clean_water = clean_water;
            _dirty_water = dirty_water;

            //the initial condition is that is no consumed water until reach a percent of fill
            _clean_water.SetFlows(_clean_pump_flow, Measure.FlowRate.ZERO);
            _dirty_water.SetFlows(Measure.FlowRate.ZERO, Measure.FlowRate.ZERO);

            _clean_water_queue = new List<TruckPipe>();
            _dirty_water_queue = new List<TruckPipe>();

            _flooding = new Measure.Volume(0, Measure.Volume.MetricType.Liter);
            _missing_water = new Measure.Volume(0, Measure.Volume.MetricType.Liter);
            _mix_produced = new Measure.Volume(0, Measure.Volume.MetricType.Barrel);
            _status = Status.Inactive;
            _waiting_time = new Measure.Time(2, Measure.Time.MetricType.Hour);
            _waiting_time_clean = _waiting_time;
            _waiting_time_dirty = _waiting_time;
            _waiting_count_clean = Measure.Time.ZERO;
            _waiting_count_dirty = Measure.Time.ZERO;
            _pipe_supply_clean = null;
            _pipe_discharge_dirty = null;
            _finished_cache = new List<TruckPipe>();
            _clean_water_percent_init = 0.8;
        }

        /// <summary>
        /// Property for get/set the Extraction pit on the zone
        /// </summary>
        public ExtractionPit ExtractionPitZone
        {
            get { return _extraction_pit; }
            set { _extraction_pit = value; }
        }

        /// <summary>
        /// Property for get/set the clean water container on the zone
        /// </summary>
        public Fluid.Container CleanWaterContainer
        {
            get { return _clean_water; }
            set { _clean_water = value; }
        }

        /// <summary>
        /// Property for get/set de dirty water container in the zone
        /// </summary>
        public Fluid.Container DirtyWaterContainer
        {
            get { return _dirty_water; }
            set { _dirty_water = value; }
        }

        /// <summary>
        /// In case that dirty container is emptied a waiting time is started by count down timer with this time
        /// Make sure that the time is suficient before the container is filled
        /// </summary>
        public Measure.Time WaitingTimeDirtyContainer
        {
            get { return _waiting_time_dirty; }
            set { _waiting_time_dirty = value; }
        }

        /// <summary>
        /// In case that clean container is filled a waiting time is started by count down timer with this time
        /// Make sure that the time is suficient before the container is emptied
        /// </summary>
        public Measure.Time WaitingTimeCleanContainer
        {
            get { return _waiting_time_clean; }
            set { _waiting_time_clean = value; }
        }

        /// <summary>
        /// Property for get the acumulated overflow of water in dirty water container
        /// </summary>
        public Measure.Volume Flooding
        {
            get { return _flooding; }
        }

        /// <summary>
        /// Property for get the acumulated unreacheable demand of water in clean water container
        /// </summary>
        public Measure.Volume MissingWater
        {
            get { return _missing_water; }
        }
    
        /// <summary>
        /// Property for get the actual mix produced by the extraction
        /// </summary>
        public Measure.Volume MixProduced
        {
            get { return _mix_produced; }
        }

        /// <summary>
        /// Property for get/set the flow for drain the clean water truck in the front and put it in clean container water
        /// </summary>
        public Measure.FlowRate CleanWaterPumpFlow
        {
            get { return _clean_pump_flow; }
            set { _clean_pump_flow = value; }
        }

        /// <summary>
        /// Property for get/set the flow the drain the dirty water container and put it in dirty water truck in the front
        /// </summary>
        public Measure.FlowRate DirtyWaterPumpFlow
        {
            get { return _dirty_pump_flow; }
            set { _dirty_pump_flow = value; }
        }

        /// <summary>
        /// Percent of clean water in container to start the extraction
        /// </summary>
        public double CleanWaterPercentInit
        {
            get { return _clean_water_percent_init; }
            set { _clean_water_percent_init = value; }
        }

        //TESTED
        /// <summary>
        /// Appends an incoming truck from other object in the enviroment, if the container of the truck
        /// is actually filled then is not appended in the queue of dirty water container, analogous to
        /// clean water container with empty containers trucks
        /// </summary>
        /// <param name="truck">Incoming truck</param>
        /// <param name="tag">Tag for appending depending if it's dirty water or fresh water</param>
        public void AppendTruck(TruckPipe truck, Fluid.FluidTags tag)
        {
            //maybe throw execption when yout try to append a truck that is filled or empty
            //beacuse we do not want zero time events, and have no sense append a filled truck
            //for fill with dirty water, or drown an empty truck on clean water
            if (tag.Equals(Fluid.FluidTags.FreshWater))
            {
                if (!truck.WaterContainer.IsEmpty())
                {
                    truck.WaterContainer.SetFlows(Measure.FlowRate.ZERO, _clean_pump_flow);
                    _clean_water_queue.Add(truck);
                    _clean_water.IFlow = _clean_pump_flow; //_clean_pump_flow + pipeflow in case
                                                           //if (_status == Status.Inactive)
                                                           //_clean_water.IFlow = _clean_pump_flow;
                }
            }
            if (tag.Equals(Fluid.FluidTags.DirtyWater))
            {
                if (!truck.WaterContainer.IsFilled())
                {
                    truck.WaterContainer.SetFlows(_dirty_pump_flow, Measure.FlowRate.ZERO);
                    _dirty_water_queue.Add(truck);
                    _dirty_water.OFlow = _dirty_pump_flow;
                    //_dirty_pump_flow + pipeflow in case
                    //if (_status == Status.Inactive)
                    //_dirty_water.OFlow = _dirty_pump_flow;
                }
            }
        }

        //TESTED
        /// <summary>
        /// Dequeue the front truck in the queue, depending on the fluid tag. This function it's
        /// thincked to be used when an fill truck or empty truck event it's emited
        /// </summary>
        /// <param name="tag">Can be fresh water or dirty water</param>
        /// <returns>null if there is no trucks, or a TruckPipe isntance whit the front</returns>
        public TruckPipe DequeueTruck(Fluid.FluidTags tag)
        {
            if (tag.Equals(Fluid.FluidTags.FreshWater))
            {
                if (_clean_water_queue.Count == 0) return null;
                TruckPipe p = _clean_water_queue.First();
                p.WaterContainer.SetFlows(Measure.FlowRate.ZERO, Measure.FlowRate.ZERO);
                _clean_water_queue.RemoveAt(0);
                if (_clean_water_queue.Count == 0) _clean_water.IFlow = Measure.FlowRate.ZERO; //PipeFlow in case 
                return p;
            }
            if (tag.Equals(Fluid.FluidTags.DirtyWater))
            {
                if (_dirty_water_queue.Count == 0) return null;
                TruckPipe p = _dirty_water_queue.First();
                p.WaterContainer.SetFlows(Measure.FlowRate.ZERO, Measure.FlowRate.ZERO);
                _dirty_water_queue.RemoveAt(0);
                if (_dirty_water_queue.Count == 0) _dirty_water.OFlow = Measure.FlowRate.ZERO; //PipeFlow in case
                return p;
            }
            return null;
        }

        //TESTED, NO BUGS DETECTED YET
        /// <summary>
        /// Very important function, fetch all posible events, finds the minimun time event, and also
        /// finds events that occur at the same time or with a percentual difference of 0.01 for avoid
        /// sequence of events that have very small time between.
        /// </summary>
        /// <param name="delta_t">Output time that stores the minimun time event(s), if there is no
        /// events then this is infinite</param>
        /// <param name="diff_percent">Not used for now</param>
        /// <returns>A list of events that occur at the same minimun time</returns>
        public List<Events> GetAllignedEventsTrigger(out Measure.Time delta_t, double diff_percent = 0)
        {
            List<Events> clean_events = new List<Events>();
            List<Events> dirty_events = new List<Events>();

            TruckPipe clean; //front truck atached in the clean water container
            TruckPipe dirty; //fron truck atached in the dirty water container
            if (_clean_water_queue.Count == 0)
                clean = null;
            else
                clean = _clean_water_queue.First();

            if (_dirty_water_queue.Count == 0)
                dirty = null;
            else
                dirty = _dirty_water_queue.First();

            #region clean_section_analisis

            Measure.Time min_clean_event;
            if (_status == Status.CleanFilled || _status == Status.CleanFilledDirtyEmpty || _status == Status.BothFilled)
            {
                //This because previously the container was filled, do not have any case
                //to check when the container is empty, because the time to wait is suficient
                //to be minimal before the container is empty, and there is no other posibilities.
                //Also the truck cannot be emptied beacause is paused
                min_clean_event = _waiting_count_clean;
                clean_events.Add(Events.CleanWaitFinish);
            }
            else
            {
                //There are various posibilities
                //first check if a truck is atached, and calculates the time to empty(beacause is not filled, remember?)
                Measure.Time clean_truck_time;
                if (clean != null)
                    clean_truck_time = clean.WaterContainer.TimeToEmptyPerFlow(_clean_pump_flow);
                else
                    clean_truck_time = Measure.Time.INFINITE;

                MDebug.WriteLine("EZ FETCH", "clean_truck_time: " + clean_truck_time.ToString());

                //Now check the poibilities in the container
                Measure.Time clean_container_time;
                Fluid.Container.Status clean_ev;
                //if is inactive, then the demand of water is 0, so only check for the initial condition
                if (_status == Status.Inactive)
                {
                    //if there was a truck, then the container is filling at _clen_pump_flow rate
                    if (clean != null)
                    {
                        if (_pipe_supply_clean != null)
                            clean_container_time = _clean_water.TimeToFillPerFlow(_clean_pump_flow + _pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this)), _clean_water_percent_init);
                        else
                            clean_container_time = _clean_water.TimeToFillPerFlow(_clean_pump_flow, _clean_water_percent_init);
                        clean_ev = Fluid.Container.Status.Fill;
                    }
                    else
                    {
                        //there is no truck, but check if there is an pipe supplying water
                        if (_pipe_supply_clean != null)
                        {
                            clean_container_time = _clean_water.TimeToFillPerFlow(_pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this)), _clean_water_percent_init);
                            clean_ev = Fluid.Container.Status.Fill;
                        }
                        else
                        {
                            // no supplies of water nothing can happend
                            clean_container_time = Measure.Time.INFINITE;
                            clean_ev = Fluid.Container.Status.Stable;
                        }
                    }
                }
                else
                {
                    Measure.FlowRate temp;
                    //check the flowing based on the supplies
                    if (clean != null)
                    {
                        if (_pipe_supply_clean != null)
                            temp = _clean_pump_flow + _pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this)) - _extraction_pit.WaterRequeriment;
                        else
                            temp = _clean_pump_flow - _extraction_pit.WaterRequeriment;
                    }
                    else
                    {
                        if (_pipe_supply_clean != null)
                            temp = _pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this)) - _extraction_pit.WaterRequeriment;
                        else
                            temp = Measure.FlowRate.ZERO - _extraction_pit.WaterRequeriment;
                    }
                    //flowing positive then the container is able to fill
                    if (temp.Value > 0)
                    {
                        //time to fill
                        clean_container_time = _clean_water.TimeToFillPerFlow(temp);
                        clean_ev = Fluid.Container.Status.Fill;
                    }
                    else if (temp.Value < 0) //flowing negative the container is drained
                    {
                        //time to drain
                        clean_container_time = _clean_water.TimeToEmptyPerFlow(new Measure.FlowRate(-1 * temp.Value, temp.Type));
                        clean_ev = Fluid.Container.Status.Drain;
                    }
                    else
                    {
                        //flowing 0, nothing can happen
                        clean_container_time = Measure.Time.INFINITE;
                        clean_ev = Fluid.Container.Status.Stable;
                    }
                }
                MDebug.WriteLine("EZ UPDATE", "clean_container_time " + clean_container_time.ToString());

                //avoid zero time and infinte time events
                if ((clean_container_time.Nequal(Measure.Time.INFINITE) && clean_container_time.Nequal(Measure.Time.ZERO)) || (clean_truck_time.Nequal(Measure.Time.INFINITE) && clean_truck_time.Nequal(Measure.Time.ZERO)))
                {
                    if (Vector3.PercentualDiff(clean_container_time.Value, clean_truck_time.Cast(clean_container_time.Type).Value) <= 0.01)
                    {
                        if (clean_ev == Fluid.Container.Status.Drain)
                        {
                            clean_events.Add(Events.CleanContainerEmpty);
                        }
                        else
                        {
                            if (_status == Status.Inactive) clean_events.Add(Events.InitialConditionAchieved);
                            else clean_events.Add(Events.CleanContainerFilled);
                        }
                        clean_events.Add(Events.TruckDrainedClean);
                        if (clean_container_time.Equals(Measure.Time.ZERO))
                            min_clean_event = clean_truck_time;
                        else
                            min_clean_event = clean_container_time;
                    }
                    else if (clean_container_time < clean_truck_time && clean_container_time.Nequal(Measure.Time.ZERO))
                    {
                        min_clean_event = clean_container_time;
                        if (clean_ev == Fluid.Container.Status.Drain)
                        {
                            clean_events.Add(Events.CleanContainerEmpty);
                        }
                        else
                        {
                            if (_status == Status.Inactive) clean_events.Add(Events.InitialConditionAchieved);
                            else clean_events.Add(Events.CleanContainerFilled);
                        }
                    }
                    else
                    {
                        if (clean_truck_time.Nequal(Measure.Time.ZERO))
                        {
                            min_clean_event = clean_truck_time;
                            clean_events.Add(Events.TruckDrainedClean);
                        }
                        else
                        {
                            min_clean_event = Measure.Time.INFINITE;
                        }
                    }
                }
                else
                {
                    min_clean_event = Measure.Time.INFINITE;
                }
            }
            #endregion

            #region dirty_section_analisis
            Measure.Time min_dirty_event;

            if (_status == Status.DirtyEmpty || _status == Status.CleanFilledDirtyEmpty || _status == Status.BothEmpty)
            {
                //The container previously was empty, so only check when the waiting time is finished
                //could not happen that the container empty, because the time of wait is suficient to avoid this
                //also the truck atached cannot be filled beacuse is in waiting mode, and no flow
                min_dirty_event = _waiting_count_dirty;
                dirty_events.Add(Events.DirtyWaitFinish);
            }
            else
            {
                //Check all the posibilities
                //first check if there is a truck atached in the dirty container
                Measure.Time dirty_truck_time;
                if (dirty != null && _status != Status.Inactive) // in this case if is inactive the truck is no filling
                    dirty_truck_time = dirty.WaterContainer.TimeToFillPerFlow(_dirty_pump_flow);
                else
                    dirty_truck_time = Measure.Time.INFINITE;
                MDebug.WriteLine("EZ UPDATE", "dirty_truck_time: " + dirty_truck_time.ToString());

                Fluid.Container.Status dirty_ev;
                Measure.Time dirty_container_time;

                if (_status == Status.Inactive)
                {
                    //beacause the container is empty, ignore the front truck, and no dirty water coming from pit
                    dirty_container_time = Measure.Time.INFINITE;
                    dirty_ev = Fluid.Container.Status.Stable;
                }
                else
                {
                    Measure.FlowRate temp;
                    Measure.FlowRate real_input;
                    //maybe check if the clean container is clean because cannot produce water if no supply water
                    if (_status == Status.BothEmpty || _status == Status.CleanEmpty || _status == Status.DirtyFilledCleanEmpty)
                        real_input = Measure.FlowRate.ZERO;
                    else
                        real_input = _extraction_pit.DirtyWaterProduced;

                    //if no truck in the front of queue
                    if (dirty != null)
                    {
                        if (_pipe_discharge_dirty != null)
                            temp = real_input - _pipe_discharge_dirty.GetDischargeFlow(_pipe_discharge_dirty.GetDischargeBranch(this)) - _dirty_pump_flow;
                        else
                            temp = real_input - _dirty_pump_flow;
                    }
                    else
                    {
                        if (_pipe_discharge_dirty != null)
                            temp = real_input - _pipe_discharge_dirty.GetDischargeFlow(_pipe_discharge_dirty.GetDischargeBranch(this));
                        else
                            temp = real_input;
                    }

                    if (temp.Value > 0) //container is filling
                    {
                        dirty_container_time = _dirty_water.TimeToFillPerFlow(temp);
                        dirty_ev = Fluid.Container.Status.Fill;
                    }
                    else if (temp.Value < 0) // container is draining
                    {
                        dirty_container_time = _dirty_water.TimeToEmptyPerFlow(new Measure.FlowRate(-1 * temp.Value, temp.Type));
                        dirty_ev = Fluid.Container.Status.Drain;
                    }
                    else //nothing can happen, because is 0
                    {
                        dirty_container_time = Measure.Time.INFINITE;
                        dirty_ev = Fluid.Container.Status.Stable;
                    }
                }
                MDebug.WriteLine("EZ UPDATE", "dirty_container_time: " + dirty_container_time.ToString());
                if ((dirty_container_time.Nequal(Measure.Time.INFINITE) && dirty_container_time.Nequal(Measure.Time.ZERO)) || (dirty_truck_time.Nequal(Measure.Time.INFINITE) && dirty_truck_time.Nequal(Measure.Time.ZERO)))
                {
                    if (Vector3.PercentualDiff(dirty_container_time.Value, dirty_truck_time.Cast(dirty_container_time.Type).Value) <= 0.01)
                    {
                        if (dirty_ev == Fluid.Container.Status.Drain)
                        {
                            dirty_events.Add(Events.DirtyContainerEmpty);
                        }
                        else
                        {
                            dirty_events.Add(Events.DirtyContainerFilled);
                        }
                        dirty_events.Add(Events.TruckFilledDirty);
                        if (dirty_container_time.Equals(Measure.Time.ZERO))
                            min_dirty_event = dirty_truck_time;
                        else
                            min_dirty_event = dirty_container_time;
                    }
                    else if (dirty_container_time < dirty_truck_time && dirty_container_time.Nequal(Measure.Time.ZERO))
                    {
                        min_dirty_event = dirty_container_time;
                        if (dirty_ev == Fluid.Container.Status.Drain)
                        {
                            dirty_events.Add(Events.DirtyContainerEmpty);
                        }
                        else
                        {
                            dirty_events.Add(Events.DirtyContainerFilled);
                        }
                    }
                    else
                    {
                        if (dirty_truck_time.Nequal(Measure.Time.ZERO))
                        {
                            min_dirty_event = dirty_truck_time;
                            dirty_events.Add(Events.TruckFilledDirty);
                        }
                        else
                        {
                            min_dirty_event = Measure.Time.INFINITE;
                        }
                    }
                }
                else
                {
                    min_dirty_event = Measure.Time.INFINITE;
                }
            }


            #endregion

            if (min_dirty_event.Nequal(Measure.Time.INFINITE) || min_clean_event.Nequal(Measure.Time.INFINITE))
            {
                if (Vector3.PercentualDiff(min_clean_event.Value, min_dirty_event.Cast(min_clean_event.Type).Value) <= 0.01)
                {
                    for (int i = 0; i < dirty_events.Count; i++)
                    {
                        clean_events.Add(dirty_events[i]);
                    }
                    delta_t = min_clean_event;
                    return clean_events;
                }
                else if (min_clean_event < min_dirty_event)
                {
                    delta_t = min_clean_event;
                    return clean_events;
                }
                else
                {
                    delta_t = min_dirty_event;
                    return dirty_events;
                }
            }
            else
            {
                delta_t = Measure.Time.INFINITE;
                clean_events.Clear();
                dirty_events.Clear();
                return null;
            }
        }

        //TESTED, NO BUGS DETECTED YET
        /// <summary>
        /// This function update the instace an amount of time, cannot be called whenever,
        /// must follow the life cicle Fetch Event -> Update -> Handle Finished -> Redistribute
        /// </summary>
        /// <param name="delta_t">Time to update</param>
        /// <param name="events">Events that occur in this time, if exist</param>
        public void UpdateDeltaTime(Measure.Time delta_t, List<Events> events = null)
        {
            MDebug.WriteLine("EZ", "UPDATING FUNCTION \n");
            if (delta_t.Equals(Measure.Time.INFINITE) || delta_t.Equals(Measure.Time.ZERO)) return;
            //if events are null or 0 no events occur in this time
            if (events == null || events.Count == 0)
            {
                if(_status != Status.Inactive && _status != Status.CleanEmpty && _status != Status.BothEmpty && _status != Status.DirtyFilledCleanEmpty)
                {
                    _mix_produced = _mix_produced + (delta_t * _extraction_pit.MixPerHour);
                }
                #region clean_section
                Measure.Volume clean_required; //water used by the pit
                Measure.Volume clean_filled;   //water dropped by the truck to the container 
                if (_status == Status.CleanFilled || _status == Status.BothFilled || _status == Status.CleanFilledDirtyEmpty)
                {
                    //only consume water from the clean container, because the truck is waiting(if exist)
                    //this is to avoid overflow in the clean container, pipe supply also is virtually closed
                    clean_required = delta_t * _clean_water.OFlow;
                    _clean_water.ActualQuantity = _clean_water.ActualQuantity - clean_required;
                    _waiting_count_clean = _waiting_count_clean - delta_t;
                }
                else
                {
                    if (_status == Status.Inactive)
                        clean_required = Measure.Volume.ZERO;
                    else
                        clean_required = delta_t * _clean_water.OFlow;

                    if (_clean_water_queue.Count == 0)
                    {
                        //there is no trucks, so water consumption from the truck is 0
                        clean_filled = Measure.Volume.ZERO;
                    }
                    else
                    {
                        //There is a truck attached, consume the water from the first truck, and update
                        TruckPipe front_truck;
                        front_truck = _clean_water_queue.First();
                        clean_filled = delta_t * _clean_pump_flow;
                        front_truck.WaterContainer.ActualQuantity = front_truck.WaterContainer.ActualQuantity - clean_filled;
                    }

                    //if exist an supply of water from pipe then consider its volume
                    if (_pipe_supply_clean != null)
                    {
                        clean_filled = clean_filled + (delta_t * _pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this)));
                        _pipe_supply_clean.Consume(delta_t * _pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this)), _pipe_supply_clean.GetSupplyBranch(this));
                    }
                    Measure.Volume diff_clean = clean_filled - clean_required; //if positive fill, if negative consume
                    Measure.Volume abs_diff = new Measure.Volume(Math.Abs(diff_clean.Value), diff_clean.Type);

                    // critical case!!! status = clean container empty, demand excced the actual quantity
                    if (_status == Status.BothEmpty || _status == Status.CleanEmpty || _status == Status.DirtyFilledCleanEmpty)
                    {
                        if (diff_clean.Value > 0) //the water dropped from the truck(and pipe if exist) is suficient to cover the demand
                        {
                            //fill the container with the diffennce of consumtion and filling
                            _clean_water.ActualQuantity = diff_clean;
                            switch (_status)
                            {
                                //check for a new status
                                case Status.BothEmpty:
                                    _status = Status.DirtyEmpty;
                                    break;
                                case Status.CleanEmpty:
                                    _status = Status.Normal;
                                    break;
                                case Status.DirtyFilledCleanEmpty:
                                    _status = Status.DirtyFilled;
                                    break;
                            }
                        }
                        else
                        {
                            //there is no suficient water to cover the demand
                            //missing water stores this amount
                            _missing_water = _missing_water + abs_diff;
                        }
                    }
                    else
                    {
                        //no critical case, update normally, cannot happen that filled or empty because an event would have been issued
                        _clean_water.ActualQuantity = _clean_water.ActualQuantity + diff_clean;
                    }
                }
                #endregion

                //dirty section update
                #region dirty_section

                Measure.Volume container_to_fill;  //dirty water produced by the pit
                Measure.Volume dirty_consumed;     //water dropped to the truck(and the pipes if exist)

                if (_status == Status.DirtyEmpty || _status == Status.BothEmpty || _status == Status.CleanFilledDirtyEmpty)
                {
                    //only fill with dirty water the container, because the truck is waiting and no filling(if exist)
                    if (_status == Status.BothEmpty) // cannot produce water if there is no water for produce
                        container_to_fill = Measure.Volume.ZERO;
                    else
                        container_to_fill = delta_t * _extraction_pit.DirtyWaterProduced;
                    _dirty_water.ActualQuantity = _dirty_water.ActualQuantity + container_to_fill;
                    _waiting_count_dirty = _waiting_count_dirty - delta_t;
                }
                else
                {
                    //cannot produce water(dirty) if is inactive, or there is no water for supply
                    if (_status == Status.Inactive || _status == Status.CleanEmpty || _status == Status.DirtyFilledCleanEmpty)
                        container_to_fill = Measure.Volume.ZERO;
                    else
                        container_to_fill = delta_t * _extraction_pit.DirtyWaterProduced; //produce its respective amount of dirty water

                    if (_dirty_water_queue.Count == 0)
                    {
                        //if no trucks the water to fill the truck is 0
                        dirty_consumed = Measure.Volume.ZERO;
                    }
                    else
                    {
                        //if exist a truck then its container is filled and updated
                        if (_status != Status.Inactive)
                        {
                            TruckPipe front_d = _dirty_water_queue.First();
                            dirty_consumed = delta_t * _dirty_pump_flow;
                            front_d.WaterContainer.ActualQuantity = front_d.WaterContainer.ActualQuantity + dirty_consumed;
                        }
                        else
                        {
                            dirty_consumed = Measure.Volume.ZERO;
                        }
                    }

                    //if exist a pipe for discharge dirty water, Consider its amount of water consumed
                    if (_pipe_discharge_dirty != null)
                    {
                        dirty_consumed = dirty_consumed + (delta_t * _pipe_discharge_dirty.GetDischargeFlow(_pipe_discharge_dirty.GetDischargeBranch(this)));
                        _pipe_discharge_dirty.Discharge(delta_t * _pipe_discharge_dirty.GetDischargeFlow(_pipe_discharge_dirty.GetDischargeBranch(this)), _pipe_discharge_dirty.GetDischargeBranch(this));
                    }

                    //difference of volume of consumed and filled, in the dirty container
                    Measure.Volume diff_d = container_to_fill - dirty_consumed;
                    Measure.Volume abs_diff_d = new Measure.Volume(Math.Abs(diff_d.Value), diff_d.Type);

                    //critical case!!! status = dirty container filled
                    if (_status == Status.BothFilled || _status == Status.DirtyFilled || _status == Status.DirtyFilledCleanEmpty)
                    {
                        //Is a critical case beacause the dirty water container is filled and the water pit still producing water
                        //inminent overflow
                        if (diff_d.Value < 0)
                        {
                            //if the difference between consumed and filled is negative then, the container does not overflow
                            _dirty_water.ActualQuantity = _dirty_water.ActualQuantity - abs_diff_d;
                            switch (_status)
                            {
                                //update the status then
                                case Status.BothFilled:
                                    _status = Status.CleanFilled;
                                    break;
                                case Status.DirtyFilled:
                                    _status = Status.Normal;
                                    break;
                                case Status.DirtyFilledCleanEmpty:
                                    _status = Status.CleanEmpty;
                                    break;
                            }
                        }
                        else
                        {
                            //consumtion is no suficient to avoid overflow, flooding stores the extra dirty water
                            _flooding = _flooding + abs_diff_d;
                        }
                    }
                    else
                    {
                        //no critical case, update normally, cannot happend that filled or empty because an event would have been issued
                        _dirty_water.ActualQuantity = _dirty_water.ActualQuantity + diff_d;
                    }
                }
                #endregion
            }
            else
            {
                if (_status != Status.Inactive && _status != Status.CleanEmpty && _status != Status.BothEmpty && _status != Status.DirtyFilledCleanEmpty)
                {
                    _mix_produced = _mix_produced + (delta_t * _extraction_pit.MixPerHour);
                }
                Measure.Volume ctruck_drained;
                Measure.Volume dtruck_filled;
                Measure.Volume clean_consumed;
                Measure.Volume dirty_generated;
                Measure.Volume pipe_supply = Measure.Volume.ZERO;
                Measure.Volume pipe_discharge = Measure.Volume.ZERO;
                TruckPipe front;
                //MDebug.WriteLine("EZ", "PIPE SUPPLY TEST: " + pipe_supply.ToString());

                //clean truck drained, if not truck or filled, etc, then no truck water consumtion
                if (_clean_water_queue.Count == 0 || _status == Status.BothFilled || _status == Status.CleanFilledDirtyEmpty || _status == Status.CleanFilled)
                {
                    ctruck_drained = Measure.Volume.ZERO;
                    if (_status == Status.BothFilled || _status == Status.CleanFilledDirtyEmpty || _status == Status.CleanFilled)
                    {
                        _waiting_count_clean = _waiting_count_clean - delta_t;
                    }
                }
                else
                {
                    // consume the required water based on time
                    //if(_pipe_supply_clean != null)
                    //    pipe_supply = delta_t * _pipe_supply_clean.GetSupplyFlow();
                    ctruck_drained = delta_t * _clean_pump_flow;
                    front = _clean_water_queue.First();
                    front.WaterContainer.ActualQuantity = front.WaterContainer.ActualQuantity - ctruck_drained;
                }

                if (_status != Status.BothFilled && _status != Status.CleanFilled && _status != Status.CleanFilledDirtyEmpty)
                {
                    if (_pipe_supply_clean != null)
                    {
                        pipe_supply = delta_t * _pipe_supply_clean.GetSupplyFlow(_pipe_supply_clean.GetSupplyBranch(this));
                        _pipe_supply_clean.Consume(pipe_supply, _pipe_supply_clean.GetSupplyBranch(this));
                    }
                }

                if (_dirty_water_queue.Count == 0 || _status == Status.Inactive || _status == Status.DirtyEmpty || _status == Status.CleanFilledDirtyEmpty || _status == Status.BothEmpty)
                {
                    dtruck_filled = Measure.Volume.ZERO;
                    if (_status == Status.DirtyEmpty || _status == Status.CleanFilledDirtyEmpty || _status == Status.BothEmpty)
                    {
                        //MDebug.WriteLine("EZ", "Waiting Dirty Count: " + delta_t.Cast(Measure.Time.MetricType.Hour).ToString());
                        _waiting_count_dirty = _waiting_count_dirty - delta_t;
                    }
                }
                else
                {
                    //if (_pipe_discharge_dirty != null)
                    //    pipe_discharge = delta_t * _pipe_discharge_dirty.GetDischargeFlow();
                    dtruck_filled = delta_t * _dirty_pump_flow;
                    front = _dirty_water_queue.First();
                    front.WaterContainer.ActualQuantity = front.WaterContainer.ActualQuantity + dtruck_filled;
                }

                if (_status != Status.Inactive && _status != Status.DirtyEmpty && _status != Status.CleanFilledDirtyEmpty && _status != Status.BothEmpty)
                {
                    if (_pipe_discharge_dirty != null)
                    {
                        pipe_discharge = delta_t * _pipe_discharge_dirty.GetDischargeFlow(_pipe_discharge_dirty.GetDischargeBranch(this));
                        _pipe_discharge_dirty.Discharge(pipe_discharge, _pipe_discharge_dirty.GetDischargeBranch(this));
                    }
                }

                if (_status != Status.Inactive) clean_consumed = delta_t * _extraction_pit.WaterRequeriment;
                else clean_consumed = Measure.Volume.ZERO;

                if (_status == Status.Inactive || _status == Status.CleanEmpty || _status == Status.BothEmpty || _status == Status.DirtyFilledCleanEmpty)
                    dirty_generated = Measure.Volume.ZERO;
                else
                    dirty_generated = delta_t * _extraction_pit.DirtyWaterProduced;

                Measure.Volume diff_clean = ctruck_drained + pipe_supply - clean_consumed;
                MDebug.WriteLine("EZ", "Clean Volume " + diff_clean.ToString());

                Measure.Volume diff_dirt = dirty_generated - dtruck_filled - pipe_discharge;
                MDebug.WriteLine("EZ", "Dirty Volume " + diff_dirt.ToString());

                if (_status == Status.CleanEmpty || _status == Status.BothEmpty || _status == Status.DirtyFilledCleanEmpty)
                {
                    if (diff_clean.Value > 0)
                    {
                        _clean_water.ActualQuantity = diff_clean;
                        switch (_status)
                        {
                            case Status.CleanEmpty:
                                _status = Status.Normal;
                                break;
                            case Status.BothEmpty:
                                _status = Status.DirtyEmpty;
                                break;
                            case Status.DirtyFilledCleanEmpty:
                                _status = Status.DirtyFilled;
                                break;
                        }
                    }
                    else
                    {
                        _missing_water = _missing_water + new Measure.Volume(Math.Abs(diff_clean.Value), diff_clean.Type);
                    }
                }
                else
                {
                    _clean_water.ActualQuantity = _clean_water.ActualQuantity + diff_clean;
                    MDebug.WriteLine("EZ", "Clean Water Q: " + _clean_water.ActualQuantity.ToString());
                }

                if (_status == Status.BothFilled || _status == Status.DirtyFilled || _status == Status.DirtyFilledCleanEmpty)
                {
                    if (diff_dirt.Value < 0)
                    {
                        _dirty_water.ActualQuantity = _dirty_water.ActualQuantity - new Measure.Volume(Math.Abs(diff_dirt.Value), diff_dirt.Type);
                        switch (_status)
                        {
                            case Status.BothFilled:
                                _status = Status.CleanFilled;
                                break;
                            case Status.DirtyFilled:
                                _status = Status.Normal;
                                break;
                            case Status.DirtyFilledCleanEmpty:
                                _status = Status.CleanEmpty;
                                break;
                        }
                    }
                    else
                    {
                        _flooding = _flooding + diff_dirt;
                    }
                }
                else
                {
                    _dirty_water.ActualQuantity = _dirty_water.ActualQuantity + diff_dirt;
                }


                for (int i = 0; i < events.Count; i++)
                {
                    switch (events[i])
                    {
                        case Events.InitialConditionAchieved:
                            _clean_water.OFlow = _extraction_pit.WaterRequeriment;   //plus pipe supply
                            _dirty_water.IFlow = _extraction_pit.DirtyWaterProduced; //plus pipe supply
                            _waiting_count_dirty = _waiting_time_dirty;
                            NewStatus(events[i]);
                            break;
                        case Events.CleanContainerEmpty:
                            _clean_water.Empty(); //assert to empty
                            NewStatus(events[i]);
                            //inminent exceding demand of water, nothing to do
                            break;
                        case Events.CleanContainerFilled:
                            _clean_water.Fill(); //assert to fill
                            NewStatus(events[i]);
                            _waiting_count_clean = _waiting_time_clean; // start the waiting time
                            break;
                        case Events.CleanWaitFinish:
                            _waiting_count_clean = Measure.Time.ZERO;
                            NewStatus(events[i]);
                            break;
                        case Events.DirtyContainerEmpty:
                            _dirty_water.Empty(); //assert to empty
                            NewStatus(events[i]);
                            _waiting_count_dirty = _waiting_time_dirty; //start wainting time
                            break;
                        case Events.DirtyContainerFilled:
                            _dirty_water.Fill(); //assert to fill
                            NewStatus(events[i]);
                            //inminent flooding in dirty container, nothing to do
                            break;
                        case Events.DirtyWaitFinish:
                            _waiting_count_dirty = Measure.Time.ZERO;
                            if (_dirty_water.ActualQuantity.Equals(Measure.Volume.ZERO)) _waiting_count_dirty = _waiting_time_dirty;
                            else NewStatus(events[i]);
                            break;
                        case Events.TruckDrainedClean:
                            //drain vehicle and detach, put it in finished
                            front = DequeueTruck(Fluid.FluidTags.FreshWater);
                            front.WaterContainer.Empty();
                            _finished_cache.Add(front);
                            break;
                        case Events.TruckFilledDirty:
                            //fill vehicle and detach
                            front = DequeueTruck(Fluid.FluidTags.DirtyWater);
                            front.WaterContainer.Fill();
                            _finished_cache.Add(front);
                            break;
                    }
                }
            }
        }

        //TESTED
        /// <summary>
        /// Updates the status based on the previous status and the event occurred
        /// </summary>
        /// <param name="ev">Event that occur</param>
        private void NewStatus(Events ev)
        {
            switch (ev)
            {
                //Check for a way to compress later
                case Events.CleanContainerEmpty:
                    switch (_status)
                    {
                        case Status.BothEmpty:
                            //never happend
                            break;
                        case Status.BothFilled:
                            _status = Status.DirtyFilledCleanEmpty;
                            break;
                        case Status.CleanEmpty:
                            //never happend
                            break;
                        case Status.CleanFilled:
                            _status = Status.CleanEmpty;
                            break;
                        case Status.CleanFilledDirtyEmpty:
                            _status = Status.BothEmpty;
                            break;
                        case Status.DirtyEmpty:
                            _status = Status.BothEmpty;
                            break;
                        case Status.DirtyFilled:
                            _status = Status.DirtyFilledCleanEmpty;
                            break;
                        case Status.DirtyFilledCleanEmpty:
                            //never happend
                            break;
                        case Status.Inactive:
                            //never happend
                            break;
                        case Status.Normal:
                            _status = Status.CleanEmpty;
                            break;
                    }
                    break;
                case Events.CleanContainerFilled:
                    switch (_status)
                    {
                        case Status.BothEmpty:
                            _status = Status.CleanFilledDirtyEmpty;
                            break;
                        case Status.BothFilled:
                            //never happend
                            break;
                        case Status.CleanEmpty:
                            _status = Status.CleanFilled;
                            break;
                        case Status.CleanFilled:
                            //never happend
                            break;
                        case Status.CleanFilledDirtyEmpty:
                            //never happend
                            break;
                        case Status.DirtyEmpty:
                            _status = Status.CleanFilledDirtyEmpty;
                            break;
                        case Status.DirtyFilled:
                            _status = Status.BothFilled;
                            break;
                        case Status.DirtyFilledCleanEmpty:
                            _status = Status.BothFilled;
                            break;
                        case Status.Inactive:
                            //never happend, initial condition in their case
                            break;
                        case Status.Normal:
                            _status = Status.CleanFilled;
                            break;
                    }
                    break;
                case Events.CleanWaitFinish: //only emited when previously the container was filled
                    switch (_status)
                    {
                        case Status.BothEmpty:
                            //never happend
                            break;
                        case Status.BothFilled:
                            _status = Status.DirtyFilled;
                            break;
                        case Status.CleanEmpty:
                            //never happend
                            break;
                        case Status.CleanFilled:
                            _status = Status.Normal;
                            break;
                        case Status.CleanFilledDirtyEmpty:
                            _status = Status.DirtyEmpty;
                            break;
                        case Status.DirtyEmpty:
                            //never happend
                            break;
                        case Status.DirtyFilled:
                            //never happend _status
                            break;
                        case Status.DirtyFilledCleanEmpty:
                            //never happend
                            break;
                        case Status.Inactive:
                            //never happend, initial condition in their case
                            break;
                        case Status.Normal:
                            //never happend
                            break;
                    }
                    break;
                case Events.DirtyContainerEmpty:
                    switch (_status)
                    {
                        case Status.BothEmpty:
                            //never happend
                            break;
                        case Status.BothFilled:
                            _status = Status.CleanFilledDirtyEmpty;
                            break;
                        case Status.CleanEmpty:
                            _status = Status.BothEmpty;
                            break;
                        case Status.CleanFilled:
                            _status = Status.CleanFilledDirtyEmpty;
                            break;
                        case Status.CleanFilledDirtyEmpty:
                            //never happend
                            break;
                        case Status.DirtyEmpty:
                            //never happend
                            break;
                        case Status.DirtyFilled:
                            _status = Status.DirtyEmpty;
                            break;
                        case Status.DirtyFilledCleanEmpty:
                            _status = Status.BothEmpty;
                            break;
                        case Status.Inactive:
                            //never happend, initial condition in their case
                            break;
                        case Status.Normal:
                            _status = Status.DirtyEmpty;
                            break;
                    }
                    break;
                case Events.DirtyContainerFilled:
                    switch (_status)
                    {
                        case Status.BothEmpty:
                            _status = Status.DirtyFilledCleanEmpty;
                            break;
                        case Status.BothFilled:
                            //never happend
                            break;
                        case Status.CleanEmpty:
                            _status = Status.DirtyFilledCleanEmpty;
                            break;
                        case Status.CleanFilled:
                            _status = Status.BothFilled;
                            break;
                        case Status.CleanFilledDirtyEmpty:
                            _status = Status.BothFilled;
                            break;
                        case Status.DirtyEmpty:
                            _status = Status.DirtyFilled;
                            break;
                        case Status.DirtyFilled:
                            //never happend
                            break;
                        case Status.DirtyFilledCleanEmpty:
                            //never happend
                            break;
                        case Status.Inactive:
                            //never happend, initial condition in their case
                            break;
                        case Status.Normal:
                            _status = Status.DirtyFilled;
                            break;
                    }
                    break;
                case Events.DirtyWaitFinish: //only emited when previously the container was empty
                    switch (_status)
                    {
                        case Status.BothEmpty:
                            _status = Status.CleanEmpty;
                            break;
                        case Status.BothFilled:
                            //never happend 
                            break;
                        case Status.CleanEmpty:
                            //never happend
                            break;
                        case Status.CleanFilled:
                            //never happend
                            break;
                        case Status.CleanFilledDirtyEmpty:
                            _status = Status.CleanFilled;
                            break;
                        case Status.DirtyEmpty:
                            _status = Status.Normal;
                            break;
                        case Status.DirtyFilled:
                            //never happend
                            break;
                        case Status.DirtyFilledCleanEmpty:
                            //never happend
                            break;
                        case Status.Inactive:
                            //never happend, initial condition in their case
                            break;
                        case Status.Normal:
                            //never happend
                            break;
                    }
                    break;
                case Events.InitialConditionAchieved:
                    _status = Status.DirtyEmpty;
                    break;
                case Events.TruckDrainedClean:
                    //Does not affect the status
                    break;
                case Events.TruckFilledDirty:
                    //does not affect the status
                    break;
            }
        }

        /// <summary>
        /// Sets the waiting time on clean water container when is filled based on a percent of its capacity
        /// </summary>
        /// <param name="percent">Percent of capacity to drain the container</param>
        public void SetWaitingCleanTimeBasedOnPercent(double percent)
        {
            if (percent > 0.0 && percent < 1.0)
            {
                Measure.Time t;
                //Assume the worst draining consumtion
                Measure.FlowRate f = _extraction_pit.WaterRequeriment;

                t = _clean_water.MaximumTimeToEmpty(f, 1 - percent);
                _waiting_time_clean = t;
            }
        }

        /// <summary>
        /// Sets the waiting time on dirty water container when is emptied based on a percent of its capacity
        /// </summary>
        /// <param name="percent">Percent of the container capacity to fill</param>
        public void SetWaitingDirtyTimeBasedOnPercent(double percent)
        {
            if (percent > 0.0 && percent < 1.0)
            {
                Measure.Time t;
                Measure.FlowRate f = _extraction_pit.DirtyWaterProduced;

                t = _dirty_water.MaximumTimeToFill(f, percent);
                _waiting_time_dirty = t;
            }
        }

        /// <summary>
        /// Set the waiting time when the containers are filled or emptied based of a percent of its 
        /// maximum capacity
        /// </summary>
        /// <param name="c_percent">Clean container percent to drain</param>
        /// <param name="d_percent">Dirty container percent to fill</param>
        public void SetsWaitingTimesBasedOnPercent(double c_percent, double d_percent)
        {
            SetWaitingCleanTimeBasedOnPercent(c_percent);
            SetWaitingDirtyTimeBasedOnPercent(d_percent);
        }

        /// <summary>
        /// Set the object interfaces for piping system used for a supply of water, or 
        /// a discharge of water, must be implemented in a class
        /// </summary>
        /// <param name="clean_supply">Interface for supply pipe</param>
        /// <param name="dirty_discharge">Interface for discharge</param>
        public void SetPipingSystems(IPipeSupply clean_supply, IPipeDischarge dirty_discharge)
        {
            _pipe_supply_clean = clean_supply;
            _pipe_discharge_dirty = dirty_discharge;
        }

        /// <summary>
        /// Json Schema for building objects 
        /// </summary>
        /// <param name="print_exception">boolean for print exceptions</param>
        public static void InitSchema(bool print_exception = false)
        {
            try
            {
                string json = File.ReadAllText(@"..\..\res\building_schemas\ez_minimal.json");
                _schema = JSchema.Parse(json);
                //Console.WriteLine(_schema.ToString());
                _schema_loaded = true;
            }
            catch (ArgumentException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (DirectoryNotFoundException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (IOException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (JSchemaException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Builds an instance from json 
        /// </summary>
        /// <param name="json_object">Json representation in string</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null is an error occur</returns>
        public static ExtractionZone BuildFromJson(string json_str, bool print_exception = false)
        {
            try
            {
                JObject obj = JObject.Parse(json_str);
                return BuildFromJson(obj);
            }
            catch(JsonReaderException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Builds an instance from a json object
        /// </summary>
        /// <param name="json_object">Json object</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null if an error occur</returns>
        public static ExtractionZone BuildFromJson(JObject json_obj, bool print_exception = false)
        {
            if (!_schema_loaded) InitSchema();
            if (_schema_loaded)
            {
                if (json_obj.IsValid(_schema))
                {
                    JObject c_cont = (JObject)json_obj["clean_container"];
                    JObject d_cont = (JObject)json_obj["dirty_container"];
                    JObject w_pit = (JObject)json_obj["extraction_pit"];
                    JObject pos = (JObject)json_obj["position"];

                    ExtractionPit pit = new ExtractionPit(new Measure.FlowRate((double)w_pit["water_demand"], Measure.FlowRate.MetricType.LiterPerSec),
                                                          new Measure.FlowRate((double)w_pit["mix_produced"], Measure.FlowRate.MetricType.LiterPerSec),
                                                          new Measure.FlowRate((double)w_pit["gas_to_extract"], Measure.FlowRate.MetricType.LiterPerSec),
                                                          new Measure.FlowRate((double)w_pit["dirtywater_produced"], Measure.FlowRate.MetricType.LiterPerSec));
                    Fluid.Container cc = new Fluid.Container(new Measure.Volume((double)c_cont["capacity"], Measure.Volume.MetricType.Liter),
                                                             new Measure.Volume((double)c_cont["actual_quantity"], Measure.Volume.MetricType.Liter));
                    Fluid.Container dc = new Fluid.Container(new Measure.Volume((double)d_cont["capacity"], Measure.Volume.MetricType.Liter),
                                                             new Measure.Volume((double)d_cont["actual_quantity"], Measure.Volume.MetricType.Liter));
                    Vector3 position = new Vector3((double)pos["x"], (double)pos["y"], (double)pos["z"]);

                    ExtractionZone ez = new ExtractionZone(null, pit, cc, dc);
                    ez.Position = position;
                    JToken checker;
                    checker = json_obj["clean_fill_flow"];
                    if (checker != null && checker.Type == JTokenType.Float)
                        ez.CleanWaterPumpFlow = new Measure.FlowRate((double)checker, Measure.FlowRate.MetricType.LiterPerSec);
                    else
                        ez.CleanWaterPumpFlow = new Measure.FlowRate(1, Measure.FlowRate.MetricType.LiterPerSec);


                    checker = json_obj["dirty_fill_flow"];
                    if (checker != null && checker.Type == JTokenType.Float)
                        ez.CleanWaterPumpFlow = new Measure.FlowRate((double)checker, Measure.FlowRate.MetricType.LiterPerSec);
                    else
                        ez.DirtyWaterPumpFlow = new Measure.FlowRate(1, Measure.FlowRate.MetricType.LiterPerSec);

                    checker = json_obj["waiting_percent_clean"];
                    if (checker != null && checker.Type == JTokenType.Float)
                        ez.SetWaitingCleanTimeBasedOnPercent((double)checker);
                    else
                        ez.SetWaitingCleanTimeBasedOnPercent(0.7);

                    checker = json_obj["waiting_percent_dirty"];
                    if (checker != null && checker.Type == JTokenType.Float)
                        ez.SetWaitingDirtyTimeBasedOnPercent((double)checker);
                    else
                        ez.SetWaitingDirtyTimeBasedOnPercent(0.4);

                    checker = json_obj["clean_water_percent_to_init"];
                    if (checker != null && checker.Type == JTokenType.Float)
                        ez.CleanWaterPercentInit = (double)checker;

                    return ez;
                }
                else
                {
                    if (print_exception) Console.WriteLine("Object does not match with schema");
                    return null;
                }

            }
            return null;
        }

        //TESTED
        /// <summary>
        /// Reads the list of truck that finished in a life cicle, and also clears the list in the instance
        /// </summary>
        /// <returns>A list of finished trucks(if exist) during life cicle</returns>
        public List<TruckPipe> VolatileFinishedRead()
        {
            if (_finished_cache.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                for (int i = 0; i < _finished_cache.Count; i++)
                {
                    ret.Add(_finished_cache[i]);
                }
                _finished_cache.Clear();
                return ret;
            }
        }

        /// <summary>
        /// Prints actual basic information of the extraction zone
        /// </summary>
        public void PrintInfo()
        {
            Console.WriteLine("Extraction Zone with id {0}", _id);
            Console.WriteLine("\t Clean Container Water Volume: {0}", _clean_water.ActualQuantity.ToString());
            Console.WriteLine("\t Dirty Container Water Volume: {0}", _dirty_water.ActualQuantity.ToString());
            Console.WriteLine("\t Number Of Clean Water Truck Queued: {0}", _clean_water_queue.Count);
            if (_clean_water_queue.Count != 0)
            {
                Console.WriteLine("\t First Truck Clean Water Container Volume: {0}", _clean_water_queue.First().WaterContainer.ActualQuantity.ToString());
            }
            Console.WriteLine("\t Number Of Dirty Water Truck Queued: {0}", _dirty_water_queue.Count);
            if (_dirty_water_queue.Count != 0)
            {
                Console.WriteLine("\t First Truck Dirty Water Container Volume: {0}", _dirty_water_queue.First().WaterContainer.ActualQuantity.ToString());
            }
            Console.WriteLine("\t Extraction Zone Status: {0}", Status2String(_status));
            Console.WriteLine("\t Flooding of dirty water: {0}", _flooding.ToString());
            Console.WriteLine("\t Unreacheable demand of water: {0}", _missing_water.ToString());
            Console.WriteLine("\t Waiting time in clean container: {0}", _waiting_count_clean.ToString());
            Console.WriteLine("\t Waiting time in dirty container: {0}", _waiting_count_dirty.ToString());
        }

        /// <summary>
        /// Prints events information
        /// </summary>
        /// <param name="delta_t">Time at occut this events</param>
        /// <param name="events">List of events</param>
        public void PrintEventsInfo(Measure.Time delta_t, List<Events> events)
        {
            if (events == null || events.Count == 0)
                Console.WriteLine("None Events Occur");
            else
            {
                Console.WriteLine("At {0} occur this events: ", delta_t.ToString());
                for(int i = 0; i < events.Count; i++)
                {
                    Console.WriteLine("\t"+Event2String(events[i]));
                }
            }
        }

        /// <summary>
        /// Generates a string information about the event
        /// </summary>
        /// <param name="ev">Event to get inoformation</param>
        /// <returns>String with detailed information about event</returns>
        public string Event2String(Events ev)
        {
            string ret;
            switch (ev)
            {
                case Events.CleanContainerEmpty:
                    ret = "Clean Container Empty";
                    break;
                case Events.CleanContainerFilled:
                    ret = "Clean Container Filled";
                    break;
                case Events.CleanWaitFinish:
                    ret = "Waiting Time Finish In Clean Container";
                    break;
                case Events.DirtyContainerEmpty:
                    ret = "Dirty Container Empty";
                    break;
                case Events.DirtyContainerFilled:
                    ret = "Dirty Container Filled";
                    break;
                case Events.DirtyWaitFinish:
                    ret = "Waiting Time Finish In Dirty Container";
                    break;
                case Events.InitialConditionAchieved:
                    ret = "Initial Condition Achieved";
                    break;
                case Events.TruckDrainedClean:
                    ret = "Clean Water Truck Container Drained";
                    break;
                case Events.TruckFilledDirty:
                    ret = "Dirty Water Truck Container Filled";
                    break;
                default:
                    ret = "None";
                    break;

            }
            return ret;
        }

        /// <summary>
        /// Convertes the status to a printeable string
        /// </summary>
        /// <param name="status">The status to get the string</param>
        /// <returns>String about the event</returns>
        public string Status2String(Status status)
        {
            string stat;
            switch (status)
            {
                case Status.BothEmpty:
                    stat = "Both Containers Are Empty";
                    break;
                case Status.BothFilled:
                    stat = "Both Container Are Filled";
                    break;
                case Status.CleanEmpty:
                    stat = "Clean Container Empty";
                    break;
                case Status.CleanFilled:
                    stat = "Clean Container Filled";
                    break;
                case Status.CleanFilledDirtyEmpty:
                    stat = "Clean Container Is Filled And Dirty Container Emty";
                    break;
                case Status.DirtyEmpty:
                    stat = "Dirty Container Is Empty";
                    break;
                case Status.DirtyFilled:
                    stat = "Dirty Container Is Filled";
                    break;
                case Status.DirtyFilledCleanEmpty:
                    stat = "Dirty Container Filled And Clean Container Empty";
                    break;
                case Status.Inactive:
                    stat = "Extraction Zone Is Inactive";
                    break;
                case Status.Normal:
                    stat = "Extraction Zone Working Normally";
                    break;
                default:
                    stat = "";
                    break;
            }
            return stat;
        }

        /// <summary>
        /// Property for get the status of the zone
        /// </summary>        
        public Status ZoneState
        {
            get { return _status; }
        }

        #region implements
        /// <summary>
        /// Implementation of the interface IPositionIdTag, ObjectId
        /// </summary>
        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// Implementation of the interface IPositionIdTag, ObjectTag
        /// </summary>
        public string ObjectTag
        {
            get { return _tag; }
        }
        /// <summary>
        /// Implementation of the interface IPositionIdTag, Position
        /// </summary>
        public Vector3 Position
        {
            get { return _extraction_pit.Position; }
            set { _extraction_pit.Position = value; }
        }
        #endregion

    }
}
