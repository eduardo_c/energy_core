﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.IO;

namespace Energy
{
    /// <summary>
    /// Class for define a water pit, and emit events,
    /// when a resource is empty the envirmoent mus quit all
    /// trucks and reasigned to others
    /// </summary>
    class WaterPit : IPositionIdTag
    {
        private static JSchema _schema;
        private static bool _schema_loaded = false;

        /// <summary>
        /// Enumeration of possible events
        /// </summary>
        public enum Events { FrontTruckFilled, WaterReserveEmpty }
        /// <summary>
        /// Enumeration of posible status
        /// </summary>
        public enum Status { Serving, Idle, Empty }
        private static int _id_counter = 0;
        private static string _tag = MasterIdTag.WaterPitTag;
        /// <summary>
        /// Container representing the available water in the resource
        /// </summary>
        private Fluid.Container _water;
        /// <summary>
        /// List of incoming trucks for supply
        /// </summary>
        private List<TruckPipe> _truck_queue_idx;
        /// <summary>
        /// List of finished trucks
        /// </summary>
        private List<TruckPipe> _finished_cache;
        /// <summary>
        /// Maybe not necesary
        /// </summary>
        private Enviroment _context;
        /// <summary>
        /// Transfer flow from water pit to trucks
        /// </summary>
        private Measure.FlowRate _transfer_flow;
        //private int _truck_idx_serving;
        private int _id;
        private Status _status;

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        private WaterPit()
        {
            _id = IdIncrement();
            _context = null;
            _water = new Fluid.Container();
            _truck_queue_idx = new List<TruckPipe>();
            _transfer_flow = new Measure.FlowRate(3, Measure.FlowRate.MetricType.LiterPerSec);
            _status = Status.Idle;
            _finished_cache = new List<TruckPipe>();
            //_truck_idx_serving = -1;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">Maybe not necesary</param>
        /// <param name="pit_water">Water pit</param>
        public WaterPit(Enviroment context, Fluid.Container pit_water)
        {
            _id = IdIncrement();
            _context = context;
            _water = pit_water;
            _truck_queue_idx = new List<TruckPipe>();
            _transfer_flow = new Measure.FlowRate(3, Measure.FlowRate.MetricType.LiterPerSec);
            _finished_cache = new List<TruckPipe>();
        }

        public WaterPit(Enviroment context, Fluid.Container pit_water, Measure.FlowRate transfer_flow)
        {
            _id = IdIncrement();
            _context = context;
            _water = pit_water;
            _truck_queue_idx = new List<TruckPipe>();
            _transfer_flow = transfer_flow;
            _finished_cache = new List<TruckPipe>();
        }

        public void SetContext(Enviroment context)
        {
            _context = context;
        }
        /// <summary>
        /// Property for get/set the container representing the available water
        /// </summary>
        public Fluid.Container WaterContainer
        {
            get { return _water; }
            set { _water = value; }
        }
        /// <summary>
        /// Property for get/set the flow of transfoer from pit to vehicle
        /// </summary>
        public Measure.FlowRate TransferFlow
        {
            get { return _transfer_flow; }
            set { _transfer_flow = value; }
        }
        /// <summary>
        /// Appends a new truck for been filled
        /// </summary>
        /// <param name="truck">truck to append</param>
        public void AppendTruck(TruckPipe truck)
        {
            if (!truck.WaterContainer.IsFilled())
            {
                _water.SetFlows(Measure.FlowRate.ZERO, _transfer_flow);
                truck.WaterContainer.SetFlows(_transfer_flow, Measure.FlowRate.ZERO);
                _truck_queue_idx.Add(truck);
                _status = Status.Serving;
            }
            //_truck_queue_idx.Enqueue(truck);
        }

        /// <summary>
        /// Quit the front truck in the queue
        /// </summary>
        /// <param name="serve">Force to fill the container of the truck</param>
        /// <returns>The first truck in the queue</returns>
        public TruckPipe DequeueTruck(bool serve = true)
        {
            if (_truck_queue_idx.Count == 0)
            {
                return null;
            }
            else
            {
                TruckPipe t = _truck_queue_idx.First();
                t.WaterContainer.SetFlows(Measure.FlowRate.ZERO, Measure.FlowRate.ZERO);
                _truck_queue_idx.RemoveAt(0);
                if (_truck_queue_idx.Count == 0) _status = Status.Idle;
                return t;
            }
        }

        /// <summary>
        /// Quit a vehicle anywhere in this water pit
        /// </summary>
        /// <param name="truck">Truck to detach</param>
        /// <returns>The same truck if is here, null otherwise</returns>
        public TruckPipe DetachVehicle(TruckPipe truck)
        {
            if(_truck_queue_idx.Count == 0 || truck == null)
            {
                return null;
            }
            else
            {
                for (int i = 0; i < _truck_queue_idx.Count; i++)
                {
                    if (_truck_queue_idx[i] == truck)
                    {
                        TruckPipe t = _truck_queue_idx[i];
                        _truck_queue_idx.RemoveAt(i);
                        if (_truck_queue_idx.Count == 0) _status = Status.Idle;
                        return t;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Quit a list of trucks 
        /// </summary>
        /// <param name="trucks">List of truck to detach(only the trucks that exist here)</param>
        /// <returns>A list of truck that actually are here</returns>
        public List<TruckPipe> DetachVehicles(List<TruckPipe> trucks)
        {
            if (trucks == null|| trucks.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                TruckPipe t;
                for(int i = 0; i < trucks.Count; i++)
                {
                    t = DetachVehicle(trucks[i]);
                    if(t != null)
                    {
                        ret.Add(t);
                    }
                }
                if (ret.Count != 0) return ret;
                else return null;
            }
        }

        /// <summary>
        /// Force to remove all the trucks waintig for water
        /// </summary>
        /// <returns>List of trucks attached</returns>
        List<TruckPipe> QuitAll()
        {
            if (_truck_queue_idx.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                for(int i = 0; i < _truck_queue_idx.Count; i++)
                {
                    ret.Add(_truck_queue_idx[i]);
                }
                _truck_queue_idx.Clear();
                return ret;
            }
        }

        /// <summary>
        /// Appends a list of trucks in the queue
        /// </summary>
        /// <param name="trucks">Trucks tu push</param>
        void PushQueue(List<TruckPipe> trucks)
        {
            if (trucks == null) return;
            else
            {
                for(int i = 0; i < trucks.Count; i++)
                {
                    _truck_queue_idx.Add(trucks[i]);
                }
            }
        }

        /// <summary>
        /// Force to refill the container for the resource
        /// </summary>
        public void RestoreResource()
        {
            _water.Fill();
            if (_truck_queue_idx.Count == 0) _status = Status.Idle;
            else _status = Status.Serving;
        }

        /// <summary>
        /// Reads the finished trucks in this instance
        /// </summary>
        /// <returns>List of trucks finished</returns>
        public List<TruckPipe> VolatileFinishedRead()
        {
            if (_finished_cache.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                for(int i = 0; i < _finished_cache.Count; i++)
                {
                    ret.Add(_finished_cache[i]);
                }
                _finished_cache.Clear();
                return ret;
            }
        }

        /// <summary>
        /// Updates an amount of time the instance, cannot be called whenever, must follow
        /// the life cycle Fetch -> Update -> Get Finished -> Redistribute
        /// </summary>
        /// <param name="delta_t">Time to update</param>
        /// <param name="events">List of events that occur in this time</param>
        public void UpdateDeltaTime(Measure.Time delta_t, List<Events> events = null)
        {
            if (_truck_queue_idx.Count == 0) return;
            if (events == null || events.Count == 0)
            {
                Measure.Volume pit_extracted = delta_t * _transfer_flow;
                _water.ActualQuantity = _water.ActualQuantity - pit_extracted;

                TruckPipe t = _truck_queue_idx.First();
                t.WaterContainer.ActualQuantity = t.WaterContainer.ActualQuantity + pit_extracted;
            }
            else
            {
                //Fill a truck or empty the reserve involves consumtion, so...
                Measure.Volume pit_extracted = delta_t * _transfer_flow;
                bool consumtion = false;
                bool empty = false;
                for (int i = 0; i < events.Count; i++)
                {
                    Events ev = events[i];
                    switch (ev)
                    {
                        case Events.FrontTruckFilled:
                            TruckPipe t = _truck_queue_idx.First();
                            t.WaterContainer.Fill();
                            _finished_cache.Add(DequeueTruck());
                            consumtion = true;
                            break;
                        case Events.WaterReserveEmpty:
                            _water.Empty();
                            _status = Status.Empty;
                            empty = true;
                            break;
                    }
                }
                if(consumtion && !empty)
                {
                    _water.ActualQuantity = _water.ActualQuantity - pit_extracted;
                }
                else if(empty && !consumtion)
                {
                    TruckPipe t = _truck_queue_idx.First();
                    t.WaterContainer.ActualQuantity = t.WaterContainer.ActualQuantity + pit_extracted;
                }
            }
        }

        /// <summary>
        /// Get the minimal time event and a list that events that occur in this same time
        /// or with a diference percent of 0.01
        /// </summary>
        /// <param name="delta_t">Output minimal time that occur this event</param>
        /// <returns>List of events that a occur in minimal time</returns>
        public List<Events> GetAllignedEventsTrigger(out Measure.Time delta_t)
        {
            if(_truck_queue_idx.Count == 0)
            {
                delta_t = Measure.Time.INFINITE;
                return null;
            }
            else
            {
                List<Events> events = new List<Events>();
                TruckPipe front = _truck_queue_idx.First();
                Measure.Time truck_fill_time = front.WaterContainer.TimeToFillPerFlow();
                Measure.Time pit_drown_time = _water.TimeToEmptyPerFlow();
                if (truck_fill_time.Nequal(Measure.Time.INFINITE) || pit_drown_time.Nequal(Measure.Time.INFINITE))
                {
                    // as any comparation with NaN is false then I supose there is no problem here
                    if (Vector3.PercentualDiff(truck_fill_time.Value, pit_drown_time.Cast(truck_fill_time.Type).Value) <= 0.01)
                    {
                        if (truck_fill_time.Nequal(Measure.Time.ZERO) && pit_drown_time.Nequal(Measure.Time.ZERO))
                        {
                            events.Add(Events.FrontTruckFilled);
                            events.Add(Events.WaterReserveEmpty);
                            delta_t = truck_fill_time;
                        }
                        else
                        {
                            delta_t = Measure.Time.INFINITE;
                        }
                    }
                    else
                    {
                        if (truck_fill_time < pit_drown_time && truck_fill_time.Nequal(Measure.Time.ZERO))
                        {
                            events.Add(Events.FrontTruckFilled);
                            delta_t = truck_fill_time;
                        }
                        else
                        {
                            if (pit_drown_time.Nequal(Measure.Time.ZERO))
                            {
                                events.Add(Events.WaterReserveEmpty);
                                delta_t = pit_drown_time;
                            }
                            else
                            {
                                delta_t = Measure.Time.INFINITE;
                            }
                        }
                    }
                    if (events.Count == 0) return null;
                    return events;
                }
                else
                {
                    delta_t = Measure.Time.INFINITE;
                    return null;
                }
            }
        }

        /// <summary>
        /// Json Schema for building objects 
        /// </summary>
        /// <param name="print_exception">boolean for print exceptions</param>
        public static void InitSchema(bool print_exception = false)
        {
            try
            {
                string json = File.ReadAllText(@"..\..\res\building_schemas\wp_minimal.json");
                _schema = JSchema.Parse(json);
                //Console.WriteLine(_schema.ToString());
                _schema_loaded = true;
            }
            catch (ArgumentException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (DirectoryNotFoundException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (IOException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (JSchemaException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Builds an instance from json 
        /// </summary>
        /// <param name="json_object">Json representation in string</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null is an error occur</returns>
        public static WaterPit BuildFromJson(string json_object, bool print_exception = false)
        {
            try
            {
                JObject obj = JObject.Parse(json_object);
                return BuildFromJson(obj, print_exception);
            }
            catch (JsonReaderException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Builds an instance from a json object
        /// </summary>
        /// <param name="json_object">Json object</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null if an error occur</returns>
        public static WaterPit BuildFromJson(JObject json_object, bool print_exception = false)
        {
            if (!_schema_loaded) InitSchema();
            if (_schema_loaded)
            {
                if (json_object.IsValid(_schema))
                {
                    double supply_flow = (double)json_object["supply_flow"];
                    JObject cont = (JObject)json_object["resource_container"];
                    JObject pos = (JObject)json_object["position"];
                    double cap = (double)cont["capacity"];
                    double act = (double)cont["actual_quantity"];
                    Fluid.Container container = new Fluid.Container(new Measure.Volume(cap, Measure.Volume.MetricType.Liter), new Measure.Volume(act, Measure.Volume.MetricType.Liter));
                    WaterPit pit = new WaterPit(null, container, new Measure.FlowRate(supply_flow, Measure.FlowRate.MetricType.LiterPerSec));
                    Vector3 position = new Vector3((double)pos["x"], (double)pos["y"], (double)pos["z"]);
                    pit.Position = position;
                    return pit;
                }
                else
                {
                    if (print_exception) Console.WriteLine("Invalid object schema");
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public void PrintInfo()
        {
            Console.WriteLine("Water Pit with ID {0} info: ", _id);
            Console.WriteLine("\t Water pit quantity: {0}", _water.ActualQuantity.ToString());
            Console.WriteLine("\t Truck attached: {0}", _truck_queue_idx.Count);
            if (_truck_queue_idx.Count != 0)
                Console.WriteLine("\t First truck container quantity: {0}", _truck_queue_idx.First().WaterContainer.ActualQuantity.ToString());
        }

        public void PrintEventsInfo(Measure.Time delta_t, List<Events> evs)
        {
            if(evs == null || evs.Count == 0)
            {
                Console.WriteLine("None event occur");
            }
            else
            {
                Console.WriteLine("At {0} occur this events: ", delta_t.ToString());
                for(int i = 0; i < evs.Count; i++)
                {
                    Console.WriteLine("\t" + evs[i].ToString());
                }
            }
        }

        public static string EventToString(Events ev)
        {
            string s = "";
            switch (ev)
            {
                case Events.FrontTruckFilled:
                    s = "Front queue truck filled";
                    break;
                case Events.WaterReserveEmpty:
                    s = "Water reserver emptied";
                    break;
            }
            return s;
        }

        #region implements
        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }

        public string ObjectTag
        {
            get { return _tag; }
        }

        public Vector3 Position { get; set; }

        #endregion
    }
}
