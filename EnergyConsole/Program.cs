﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Schema;

namespace Energy
{
    class Program
    {
        static void Main(string[] args)
        {
            TestCheckFactibility();
            //TestTotalDistance();
            //TestPointBetwenDistance();
            //TestFindPontAtDistance();
            //TestHaversineDistance();
            //TestRoutTransition3();
            //TestExtractionZone();
            //TestRouteTransition();
            //TestRouteTransition2();
            //TestGasStation();
            //TestWaterPit();
            //TestTreatmentPlant();
        }

        public static void TestTotalDistance()
        {
            List<Vector3> path = new List<Vector3>();
            path.Add(new Vector3(21.022776, -89.625630, 0));
            path.Add(new Vector3(21.022088, -89.625488, 0));
            path.Add(new Vector3(21.021371, -89.625378, 0));
            path.Add(new Vector3(21.020103, -89.625158, 0));
            path.Add(new Vector3(20.997925, -89.621411, 0));

            List<Measure.Velocity> velocities = new List<Measure.Velocity>();
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));

            Route.PivotMarkerHelper pvt1 = new Route.PivotMarkerHelper(path.First());
            Route.PivotMarkerHelper pvt2 = new Route.PivotMarkerHelper(path.Last());

            Route route = new Route(path, pvt1, pvt2);
            route.SetVelocityConstrains(velocities);
            route.EnableDisableVelConstraint(false);
            Console.WriteLine("Distancia de la ruta: {0} mts", route.TotalDistanceInMts(RouteTransition.Transit.OneToTwo));
            Console.ReadKey();
        }

        public static void TestCheckFactibility()
        {
            List<Vector3> path = new List<Vector3>();
            path.Add(new Vector3(21.022776, -89.625630, 0));
            path.Add(new Vector3(21.022088, -89.625488, 0));
            path.Add(new Vector3(21.021371, -89.625378, 0));
            path.Add(new Vector3(21.020103, -89.625158, 0));
            path.Add(new Vector3(20.997925, -89.621411, 0));

            List<Measure.Velocity> velocities = new List<Measure.Velocity>();
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));

            Route.PivotMarkerHelper pvt1 = new Route.PivotMarkerHelper(path.First());
            Route.PivotMarkerHelper pvt2 = new Route.PivotMarkerHelper(path.Last());

            Route route = new Route(path, pvt1, pvt2);
            route.SetVelocityConstrains(velocities);
            route.EnableDisableVelConstraint(false);

            RouteTransition rt = new RouteTransition(null, route);

            TruckPipe tp = new TruckPipe(
                new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour),
                new Measure.VolumePerDistance(0.125, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
                new Fluid.Container(new Measure.Volume(80, Measure.Volume.MetricType.Liter),
                new Measure.Volume(0.25, Measure.Volume.MetricType.Liter)),
                new Fluid.Container(new Measure.Volume(), new Measure.Volume(), Fluid.FluidTags.FreshWater));
            tp.VehicleStatus = Vehicle.Status.Moving;
            tp.MaxVel = new Measure.Velocity(120, Measure.Velocity.MetricType.KMeterPerHour);

            rt.AttachVehicle(tp, pvt1);
            rt.PrintVehicleInfo();
            Console.WriteLine("Distancia total de la ruta: {0} mts", route.TotalDistanceInMts(RouteTransition.Transit.OneToTwo));
            Console.WriteLine("Gasolina dispobible: {0}\nRendimiento de combustible: {1}\nDistancia que puede recorrer el vehiculo {2}", 
                tp.GasTank.ActualQuantity,
                tp.GasConsumption,
                tp.ReachableDistance().Cast(Measure.Length.MetricType.Meter));
            if (rt.CheckFactibility(tp, rt.GetRoute().Object1))
            {
                Console.WriteLine("La ruta es alcanzable");
            }
            else
            {
                Console.WriteLine("La ruta no es alcanzable");
            }
            Console.ReadKey();
        }

        public static void TestPointBetwenDistance()
        {
            Vector3 src = new Vector3(21.026276, -89.624482, 0);
            Vector3 dst = new Vector3(21.026607, -89.624539, 0);
            Vector3 point = Vector3.PointBetweenDistance(src, dst, Vector3.HaversineDistanceInMts(src, dst)/2);
            Console.WriteLine(point);
            Console.ReadKey();
        }

        public static void TestFindPontAtDistance()
        {
            Vector3 v1 = new Vector3(21.025344, -89.607548, 0);
            Vector3 v2 = new Vector3(21.025260, -89.606507, 0);
            double deg = 540;
            double kms = 0.5;

            Vector3 v3 = Vector3.FindPointAtDistanceFrom(v1, deg, kms);
            Console.WriteLine(v3);
            Console.ReadKey();
        }

        public static void TestMeasure()
        {
            Measure.Time t = new Measure.Time(10, Measure.Time.MetricType.Second);
            t = t.Cast(Measure.Time.MetricType.Hour);
            Measure.FlowRate f = new Measure.FlowRate(1);
            f = f.Cast(Measure.FlowRate.MetricType.LiterPerSec);
            f = f.Cast(Measure.FlowRate.MetricType.CubicMeterPerSec);
            Measure.Volume v = t * f;
            v = v.Cast(Measure.Volume.MetricType.CubicMeter);
            v = v.Cast(Measure.Volume.MetricType.Liter);
            Measure.Time t2 = v / f;
            MDebug.WriteLine("MTEST", t2.ToString());
            MDebug.SetMode(false);
        }

        public static void TestHaversineDistance()
        {
            Vector3 v1 = new Vector3(21.026793, -89.594202, 0);
            Vector3 v2 = new Vector3(21.022396, -89.621352, 0);
            Console.WriteLine("Iniciando Haversine Distance test");
            Console.WriteLine("Punto 1: lat {0} lon {1} \nPunto 2: lat {2} lon {3}", v1.X, v1.Y, v2.X, v2.Y);
            Console.WriteLine("Distancia: {0} kms", Vector3.HaversineDistanceInKm(v1, v2));
            Console.ReadKey();
        }

        public static void TestRoutTransition3()
        {
            List<Vector3> path = new List<Vector3>();
            path.Add(new Vector3(21.022776, -89.625630, 0));
            path.Add(new Vector3(21.022088, -89.625488, 0));
            path.Add(new Vector3(21.021371, -89.625378, 0));
            path.Add(new Vector3(21.020103, -89.625158, 0));

            List<Measure.Velocity> velocities = new List<Measure.Velocity>();
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));

            Route.PivotMarkerHelper pvt1 = new Route.PivotMarkerHelper(path.First());
            Route.PivotMarkerHelper pvt2 = new Route.PivotMarkerHelper(path.Last());

            Route route = new Route(path, pvt1, pvt2);
            route.SetVelocityConstrains(velocities);
            route.EnableDisableVelConstraint(false);

            RouteTransition rt = new RouteTransition(null, route);

            TruckPipe tp = new TruckPipe(
                new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour),
                new Measure.VolumePerDistance(0.125, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
                new Fluid.Container(new Measure.Volume(80, Measure.Volume.MetricType.Liter), 
                new Measure.Volume(80, Measure.Volume.MetricType.Liter)),
                new Fluid.Container(new Measure.Volume(), new Measure.Volume(), Fluid.FluidTags.FreshWater));
            tp.VehicleStatus = Vehicle.Status.Moving;
            tp.MaxVel = new Measure.Velocity(120, Measure.Velocity.MetricType.KMeterPerHour);

            TruckPipe tp2 = tp.Clone();

            rt.AttachVehicle(tp, pvt1);
            //rt.AttachVehicle(tp2, pvt2);
            rt.PrintVehicleInfo();


            rt.update(new Measure.Time(5, Measure.Time.MetricType.Second));
            rt.PrintVehicleInfo();
            rt.update(new Measure.Time(5, Measure.Time.MetricType.Second));
            rt.PrintVehicleInfo();
            rt.update(new Measure.Time(5, Measure.Time.MetricType.Second));
            rt.PrintVehicleInfo();
            rt.update(new Measure.Time(5, Measure.Time.MetricType.Second));
            rt.PrintVehicleInfo();
            rt.update(new Measure.Time(5, Measure.Time.MetricType.Second));
            rt.PrintVehicleInfo();

            Console.ReadKey();
        }

        public static void TestRouteTransition2()
        {
            Console.WriteLine("Iniciando test");

            List<Vector3> path = new List<Vector3>();
            path.Add(new Vector3(0, 0, 0));
            path.Add(new Vector3(500, 1000, 0));
            path.Add(new Vector3(1000, 1000, 0));
            path.Add(new Vector3(2000, 2000, 0));
            path.Add(new Vector3(3000, 500, 0));

            List<Measure.Velocity> velocities = new List<Measure.Velocity>();
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(80, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(60, Measure.Velocity.MetricType.KMeterPerHour));

            Route.PivotMarkerHelper pvt1 = new Route.PivotMarkerHelper(path.First());
            Route.PivotMarkerHelper pvt2 = new Route.PivotMarkerHelper(path.Last());

            Route route = new Route(path, pvt1, pvt2);
            route.SetVelocityConstrains(velocities);
            route.EnableDisableVelConstraint(false);

            RouteTransition rt = new RouteTransition(null, route);

            TruckPipe tp = new TruckPipe(
                new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour),
                new Measure.VolumePerDistance(0.125, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
                new Fluid.Container(new Measure.Volume(80, Measure.Volume.MetricType.Liter), new Measure.Volume(80, Measure.Volume.MetricType.Liter)),
                new Fluid.Container(new Measure.Volume(), new Measure.Volume(), Fluid.FluidTags.FreshWater));
            tp.VehicleStatus = Vehicle.Status.Moving;
            tp.MaxVel = new Measure.Velocity(120, Measure.Velocity.MetricType.KMeterPerHour);

            TruckPipe tp2 = tp.Clone();

            rt.AttachVehicle(tp, pvt1);
            rt.AttachVehicle(tp2, pvt2);
            rt.PrintVehicleInfo();

            Console.ReadKey();
        }

        public static void TestWaterPit()
        {
            
            Console.WriteLine("CREATING A GAS WATER PIT SUPPLY \n");


            TruckPipe root = new TruckPipe(new Measure.Velocity(10, Measure.Velocity.MetricType.KMeterPerHour),
                                           new Measure.VolumePerDistance(0.04, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
                                           new Fluid.Container(new Measure.Volume(80, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO),
                                           new Fluid.Container(new Measure.Volume(10000, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO));
            string wp_json = @"{
                'resource_container':{
                    'capacity': 10000000,
                    'actual_quantity': 10000000
                },
                'supply_flow': 2,
                'position':{
                    'x': 0,
                    'y': 0,
                    'z': 0
                }
            }";
            WaterPit wp = WaterPit.BuildFromJson(wp_json, true);
            if (wp == null) return;
            //WaterPit wp = new WaterPit(null, new Fluid.Container(new Measure.Volume(30), new Measure.Volume(20)));
            for(int i = 0; i < 10; i++)
            {
                wp.AppendTruck(root.Clone());
            }
            bool exit = false;
            Measure.Time delta_t;
            List<WaterPit.Events> evs;
            wp.PrintInfo();
            while (!exit)
            {
                evs = wp.GetAllignedEventsTrigger(out delta_t);
                wp.PrintEventsInfo(delta_t, evs);
                string line;
                line = Console.ReadLine();
                if(line == "X" || line == "x")
                {
                    Console.WriteLine("Time divided");
                    wp.UpdateDeltaTime(delta_t / 2);
                }
                else
                {
                    if (line == "E" || line == "e") exit = true;
                    wp.UpdateDeltaTime(delta_t, evs);
                }
                wp.PrintInfo();
                Console.ReadKey();
            }
        }

        public static void TestRouteTransition()
        {
            Console.WriteLine("CREATING A ROUTE TRANSITION TEST \n");

            List<Vector3> testpath = new List<Vector3>();
            testpath.Add(new Vector3(0, 0, 0));
            testpath.Add(new Vector3(500, 1000, 0));
            testpath.Add(new Vector3(1000, 1000, 0));
            testpath.Add(new Vector3(2000, 2000, 0));
            testpath.Add(new Vector3(3000, 500, 0));

            List<Measure.Velocity> velocities = new List<Measure.Velocity>();
            velocities.Add(new Measure.Velocity(40, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(80, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour));
            velocities.Add(new Measure.Velocity(80, Measure.Velocity.MetricType.KMeterPerHour));


            Route.PivotMarkerHelper obj1 = new Route.PivotMarkerHelper(testpath.First());
            Route.PivotMarkerHelper obj2 = new Route.PivotMarkerHelper(testpath.Last());

            Route test_route = new Route(testpath, obj1, obj2);
            //test_route.SetVelocityConstrains(velocities);

            string jsont = @"{
                'water_container': {
                    'capacity': 10000,
                    'actual_quantity': 10000,
                    'fluid_type': 'CLEAN_WATER'
                },
                'gas_tank': {
                    'capacity': 800,
                    'actual_quantity': 800
                },
                'velocity': 50,
                'gas_efficiency':{
                    'value': 8,
                    'measure': 'GallonPerMille'
                },
                'max_vel': 70
            }";

            TruckPipe tp = TruckPipe.BuildFromJson(jsont, true);
            if (tp == null) return;

            //TruckPipe tp = new TruckPipe(new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour),
            //                             new Measure.VolumePerDistance(8, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
            //                             new Fluid.Container(new Measure.Volume(800, Measure.Volume.MetricType.Liter), new Measure.Volume(800, Measure.Volume.MetricType.Liter)),
            //                             new Fluid.Container(new Measure.Volume(10000, Measure.Volume.MetricType.Liter), new Measure.Volume(10000, Measure.Volume.MetricType.Liter)));

            RouteTransition rt = new RouteTransition(null, test_route);
            rt.GetRoute().SetVelocityConstrains(velocities);
            rt.GetRoute().EnableDisableVelConstraint(false);
            rt.AttachVehicle(tp, obj1);
            tp.VehicleStatus = Vehicle.Status.Moving;
            TruckPipe tp2 = tp.Clone();
            rt.AttachVehicle(tp2, obj2);
            rt.PrintVehicleInfo();

            List<TruckPipe> output;
            Measure.Time delta_t;
            //Console.WriteLine("Fetching event 0... \n");
            List<RouteTransition.Events> ev;
            ev = rt.GetAllignedEventsTrigger(out output, out delta_t);
            rt.PrintEventInfo(output, ev, delta_t);

            rt.UpdateDeltaTime(delta_t, output, ev);
            rt.PrintVehicleInfo();

            Console.WriteLine("Ataching new vehicle");
            rt.AttachVehicle(tp2.Clone(), obj1);
            rt.PrintVehicleInfo();

            bool exit = false;
            while (!exit)
            {
                ev = rt.GetAllignedEventsTrigger(out output, out delta_t);
                rt.PrintEventInfo(output, ev, delta_t);
                Console.WriteLine("Press X/x to delay \n");
                string line = Console.ReadLine();
                if (line.Equals("X") || line.Equals("x"))
                {
                    Console.WriteLine("Time divided");
                    rt.UpdateDeltaTime(delta_t / 2);
                }
                else
                {
                    if (line.Equals("E") || line.Equals("e")) exit = true;
                    rt.UpdateDeltaTime(delta_t, output, ev);
                }
                rt.PrintVehicleInfo();
            }

            //create a truck with only 2 liters of gas for test
            // gas efficiency 4 gal / 100 mi
            TruckPipe tp3 = new TruckPipe(new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour),
                                          new Measure.VolumePerDistance(0.04, Measure.VolumePerDistance.MetricType.GallonPerMille),
                                          new Fluid.Container(new Measure.Volume(400, Measure.Volume.MetricType.Liter), new Measure.Volume(0.1, Measure.Volume.MetricType.Liter)),
                                          new Fluid.Container(new Measure.Volume(10000, Measure.Volume.MetricType.Liter), new Measure.Volume(10000, Measure.Volume.MetricType.Liter)));

            List<Vector3> next_path = new List<Vector3>();
            next_path.Add(obj2.Position);
            next_path.Add(new Vector3(3500, 2000, 0));
            next_path.Add(new Vector3(4500, 4500, 0));
            next_path.Add(new Vector3(5000, 5000, 0));
            Route.PivotMarkerHelper obj3 = obj2;
            Route.PivotMarkerHelper obj4 = new Route.PivotMarkerHelper(next_path.Last());

            Route next_route = new Route(next_path, obj3, obj4);

            Measure.Length total = rt.GetRoute().TotalDistance(RouteTransition.Transit.OneToTwo);
            Console.Write("Total distance of route {0} \n", total.ToString());
            Console.Write("Next route distance {0} \n", next_route.TotalDistance(RouteTransition.Transit.OneToTwo).ToString());

            Console.WriteLine("Truck reacheable distance {0} \n", tp3.ReachableDistance().ToString());
            if (rt.CheckFactibility(tp3, rt.GetRoute().Object1))
            {
                Console.Write("Truck can reach the destiny \n");
            }
            else
            {
                Console.Write("Truck cannot reach the destiny \n");
            }

            if (rt.CheckPostFactibility(tp3, rt.GetRoute().Object1, next_route))
            {
                Console.Write("Truck can reach the destiny \n");
            }
            else
            {
                Console.Write("Truck cannot reach the destiny \n");
            }

            //now with a truck with full tank
            tp3.GasTank.Fill();
            Console.WriteLine("Truck reacheable distance {0} \n", tp3.ReachableDistance().ToString());
            if (rt.CheckFactibility(tp3, rt.GetRoute().Object1))
            {
                Console.Write("Truck can reach the destiny \n");
            }
            else
            {
                Console.Write("Truck cannot reach the destiny \n");
            }

            if (rt.CheckPostFactibility(tp3, rt.GetRoute().Object1, next_route))
            {
                Console.Write("Truck can reach the destiny \n");
            }
            else
            {
                Console.Write("Truck cannot reach the destiny \n");
            }

            
        }

        public static void TestExtractionZone()
        {
            Console.WriteLine("CREATING TEST EXTRACTION ZONE");
            string ezjson = @"{
                'name': 'EXTRACTION_ZONE',
                'clean_container': {
                    'capacity': 100000,
                    'actual_quantity': 0
                },
                'dirty_container': {
                    'capacity': 100000,
                    'actual_quantity': 0
                },
                'waiting_percent_clean': 0.8,
                'waiting_percent_dirty': 0.2,
                'dirty_drain_flow': 1,
                'clean_fill_flow': 1,
                'clean_water_percent_to_init': 0.5,
                'extraction_pit': {
                    'water_demand': 1,
                    'dirtywater_produced': 1,
                    'mix_produced': 0.2,
                    'gas_to_extract': 0.1
                },
                'position': {
                    'x': 0.2,
                    'y': 0.4,
                    'z': 0.5
                 }
            }";
            ExtractionZone test_zone = ExtractionZone.BuildFromJson(ezjson);
            if (test_zone == null) return;
            //ExtractionPit test_pit = new ExtractionPit(new Measure.FlowRate(1, Measure.FlowRate.MetricType.LiterPerSec),
            //                                           new Measure.FlowRate(0.9, Measure.FlowRate.MetricType.LiterPerSec),
            //                                           new Measure.FlowRate(0.2, Measure.FlowRate.MetricType.LiterPerSec),
            //                                           new Measure.FlowRate(0.6, Measure.FlowRate.MetricType.LiterPerSec));
            //
            //ExtractionZone test_zone = new ExtractionZone(null,
            //                                              test_pit,
            //                                              new Fluid.Container(new Measure.Volume(100000, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO.Cast(Measure.Volume.MetricType.Liter)),
            //                                              new Fluid.Container(new Measure.Volume(100000, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO.Cast(Measure.Volume.MetricType.Liter)));
            //
            
            TruckPipe root = new TruckPipe(new Measure.Velocity(10, Measure.Velocity.MetricType.KMeterPerHour),
                                           new Measure.VolumePerDistance(10, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
                                           new Fluid.Container(new Measure.Volume(80, Measure.Volume.MetricType.Liter), new Measure.Volume(70, Measure.Volume.MetricType.Liter)),
                                           new Fluid.Container(new Measure.Volume(10000, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO));

            //List<TruckPipe> trucks_dirty = new List<TruckPipe>();
            //List<TruckPipe> trucks_clean = new List<TruckPipe>();
            for (int i = 0; i < 25; i++)
            {
                TruckPipe truck = root.Clone();
                truck.WaterContainer.Fill();
                test_zone.AppendTruck(truck, Fluid.FluidTags.FreshWater);
            }

            for (int i = 0; i < 25; i++)
            {
                TruckPipe truck = root.Clone();
                truck.WaterContainer.Empty();
                test_zone.AppendTruck(truck, Fluid.FluidTags.DirtyWater);
            }
            test_zone.SetsWaitingTimesBasedOnPercent(0.7, 0.4);
            TrivialPipeSystem pipes = new TrivialPipeSystem();
            test_zone.SetPipingSystems(pipes, pipes);

            List<ExtractionZone.Events> ez_events;
            Measure.Time delta_t;
            test_zone.PrintInfo();
            ez_events = test_zone.GetAllignedEventsTrigger(out delta_t);
            test_zone.PrintEventsInfo(delta_t, ez_events);
            test_zone.UpdateDeltaTime(delta_t, ez_events);
            test_zone.PrintInfo();
            ConsoleKeyInfo key;
            Measure.Time tact;
            bool condition = false;
            while (!condition)
            {
                ez_events = test_zone.GetAllignedEventsTrigger(out delta_t);
                test_zone.PrintEventsInfo(delta_t, ez_events);
                Console.WriteLine("Press X/x to delay \n");
                string line = Console.ReadLine();
                if (line.Equals("X") || line.Equals("x"))
                {
                    Console.WriteLine("Time divided");
                    tact = delta_t / 2;
                    test_zone.UpdateDeltaTime(tact);
                }
                else
                {
                    if (line.Equals("E") || line.Equals("e")) condition = true;
                    tact = delta_t;
                    test_zone.UpdateDeltaTime(tact, ez_events);
                }
                test_zone.PrintInfo();
                Console.ReadKey();
            }
        }

        public static void TestGasStation()
        {
            Console.WriteLine("CREATING A GAS STATION \n");
            TruckPipe root = new TruckPipe(new Measure.Velocity(10, Measure.Velocity.MetricType.KMeterPerHour),
                                           new Measure.VolumePerDistance(0.04, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
                                           new Fluid.Container(new Measure.Volume(80, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO),
                                           new Fluid.Container(new Measure.Volume(10000, Measure.Volume.MetricType.Liter), Measure.Volume.ZERO));
            GasStation gas_station = new GasStation(4, new Measure.FlowRate(0.5, Measure.FlowRate.MetricType.LiterPerSec));
            TruckPipe d = root.Clone();
            d.GasTank.ActualQuantity = new Measure.Volume(25.6, Measure.Volume.MetricType.Liter);
            gas_station.AttachVehicle(d);
            for (int i = 0; i < 9; i++)
            {
                gas_station.AttachVehicle(root.Clone());
            }
            gas_station.PrintInfo();
            List<GasStation.Events> evs;
            List<TruckPipe> triggers;
            Measure.Time delta_t;
            bool exit = false;
            while (!exit)
            {
                evs = gas_station.GetAllignedEventsTrigger(out delta_t, out triggers);
                gas_station.PrintEventInfo(delta_t, evs, triggers);
                Console.WriteLine("Press X/x to delay \n");
                string line = Console.ReadLine();
                if (line.Equals("X") || line.Equals("x"))
                {
                    Console.WriteLine("Time divided");
                    gas_station.UpdateDeltaTime(delta_t / 2);
                }
                else
                {
                    if (line.Equals("E") || line.Equals("e")) exit = true;
                    gas_station.UpdateDeltaTime(delta_t, triggers, evs);
                }
                gas_station.PrintInfo();
            }
        }

        public static void TestTreatmentPlant()
        {
            string wt_json = @"{
                'drain_flow': 3.5,
                'position': {
                    'x': 0,
                    'y': 0,
                    'z': 0
                }
            }";

            //WaterTreatmentPlant wt = new WaterTreatmentPlant();
            WaterTreatmentPlant wt = WaterTreatmentPlant.BuildFromJson(wt_json, true);
            if (wt == null) return;

            string jsont = @"{
                'water_container': {
                    'capacity': 10000,
                    'actual_quantity': 10000,
                    'fluid_type': 'CLEAN_WATER'
                },
                'gas_tank': {
                    'capacity': 800,
                    'actual_quantity': 800
                },
                'velocity': 50,
                'gas_efficiency':{
                    'value': 8,
                    'measure': 'GallonPerMille'
                }
            }";

            TruckPipe tp = TruckPipe.BuildFromJson(jsont);
            //TruckPipe tp = new TruckPipe(new Measure.Velocity(50, Measure.Velocity.MetricType.KMeterPerHour),
            //                             new Measure.VolumePerDistance(8, Measure.VolumePerDistance.MetricType.LitersPerKMeter),
            //                             new Fluid.Container(new Measure.Volume(800, Measure.Volume.MetricType.Liter), new Measure.Volume(800, Measure.Volume.MetricType.Liter)),
            //                             new Fluid.Container(new Measure.Volume(10000, Measure.Volume.MetricType.Liter), new Measure.Volume(10000, Measure.Volume.MetricType.Liter)));
            TruckPipe t = tp.Clone();
            Measure.Volume dif = new Measure.Volume(500, Measure.Volume.MetricType.Liter);
            for (int i = 0; i < 5; i++)
            {
                t.WaterContainer.ActualQuantity = t.WaterContainer.ActualQuantity - dif;
                wt.AttachTruck(t);
                t = t.Clone();
            }
            wt.PrintInfo();
            List<WaterTreatmentPlant.Events> evs;
            List<TruckPipe> triggers;
            Measure.Time delta_t;
            bool exit = false;
            while (!exit)
            {
                evs = wt.GetAllignedEventsTrigger(out delta_t, out triggers);
                wt.PrintEventInfo(delta_t, evs, triggers);
                Console.WriteLine("Press X/x to delay \n");
                string line = Console.ReadLine();
                if (line.Equals("X") || line.Equals("x"))
                {
                    Console.WriteLine("Time divided");
                    wt.UpdateDeltaTime(delta_t / 2);
                }
                else
                {
                    if (line.Equals("E") || line.Equals("e")) exit = true;
                    wt.UpdateDeltaTime(delta_t, triggers);
                }
                wt.PrintInfo();
            }
        }
    }
}
