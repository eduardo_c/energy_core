﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
    class CostlyObject
    {
        protected double _cost;
        
        public CostlyObject()
        {
            _cost = 0;
        }

        public CostlyObject(double cost)
        {
            _cost = cost;
        }

        public double Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
    }
}
