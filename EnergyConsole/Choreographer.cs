﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
    interface Choreographer
    {
        double MasterTimeCounter { get; set; }
        void GetNextEvent(); //??
        void Update(); //??
    }
}
