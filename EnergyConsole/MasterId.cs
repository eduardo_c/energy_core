﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Energy
{
    static class MasterIdTag
    {
        static private int ObjectIdCount;
        static private int UserIdCount;
        static private TupleList<int, int> UserObjects;


        static public string VehicleTag = "VEHICLE";
        static public string RouteTag = "ROUTE";
        static public string WaterPitTag = "WATER_PIT";
        static public string ExtractionPitTag = "EXTRACTION_PIT";
        static public string RouteTransitionTag = "ROUTE_TRANSITION";
        static public string ExtractionZoneTag = "EXTRACTION_ZONE";
        static public string TreatmentPlantTag = "TREATMENT_PLANT";
        static public string PipeChainTag = "PIPE_CHAIN";
        static public string GasStation = "GAS_STATION";
        static public string PivotMarker = "PIVOT_MARKER";
    }
}
