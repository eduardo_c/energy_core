﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
   /// <summary>
   /// Class for represent a 3D Vector
   /// </summary>
    class Vector3
    {
        private static readonly int R = 6371;
        public static readonly Vector3 ZERO = new Vector3(0, 0, 0);
        /// <summary>
        /// X coodintate value
        /// </summary>
        private double _x;
        /// <summary>
        /// Y coodintate value
        /// </summary>
        private double _y;
        /// <summary>
        /// Z coodintate value
        /// </summary>
        private double _z;


        /// <summary>
        /// Constructor
        /// </summary>
        public Vector3()
        {
            _x = _y = _z = 0;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">The X coordinate value</param>
        /// <param name="y">The Y coordinate value</param>
        /// <param name="z">The Z coordinate value</param>
        public Vector3(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        /// <summary>
        /// This property sets/gets the value of the X coordinate
        /// </summary>
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        /// <summary>
        /// This property sets/gets the value of the Y coordinate
        /// </summary>
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        /// <summary>
        /// This property sets/gets the value of the Z coordinate
        /// </summary>
        public double Z
        {
            get { return _z; }
            set { _z = value; }
        }

        /// <summary>
        /// Calculates the distance between two 3D Vectors
        /// </summary>
        /// <param name="v1">Vector 1</param>
        /// <param name="v2">Vector 2</param>
        /// <returns> The distance between both vectors </returns>
        public static double Distance(Vector3 v1, Vector3 v2)
        {
            return Math.Sqrt((v1.X - v2.X) * (v1.X - v2.X) + (v1.Y - v2.Y) * (v1.Y - v2.Y) + (v1.Z - v2.Z) * (v1.Z - v2.Z));
        }

        /// <summary>
        /// Calcula la distancia en metros entre dos corrdendas en latitud y longitud
        /// usando la formula de Haversine
        /// </summary>
        /// <param name="v1">Vector que contiene la latitud y longitud</param>
        /// <param name="v2">Vector que contiene la latitud y longitud</param>
        /// <returns>distancia en metros entre los puntos</returns>
        public static double HaversineDistanceInKm(Vector3 v1, Vector3 v2)
        {
            double dLat = degToRad(v2.X - v1.X);
            double dLon = degToRad(v2.Y - v1.Y);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Cos(degToRad(v1.X)) * Math.Cos(degToRad(v2.X)) *
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return R * c;
        }

        public static double HaversineDistanceInMts(Vector3 v1, Vector3 v2)
        {
            return HaversineDistanceInKm(v1, v2) * 1000;
        }

        public static Vector3 FindPointAtDistanceFrom(Vector3 startPoint, double initialBearingDeg, double distanceKilometres)
        {
            var initialBearingRadians = degToRad(initialBearingDeg);
            var distRatio = distanceKilometres / R;
            var distRatioSine = Math.Sin(distRatio);
            var distRatioCosine = Math.Cos(distRatio);

            var startLatRad = degToRad(startPoint.X);
            var startLonRad = degToRad(startPoint.Y);

            var startLatCos = Math.Cos(startLatRad);
            var startLatSin = Math.Sin(startLatRad);

            var endLatRads = Math.Asin((startLatSin * distRatioCosine) + (startLatCos * distRatioSine * Math.Cos(initialBearingRadians)));

            var endLonRads = startLonRad
                + Math.Atan2(
                    Math.Sin(initialBearingRadians) * distRatioSine * startLatCos,
                    distRatioCosine - startLatSin * Math.Sin(endLatRads));

            return new Vector3(radToDeg(endLatRads), radToDeg(endLonRads), 0);
        }

        private static double degToRad(double deg)
        {
            return deg * (Math.PI / 180);
        }

        private static double radToDeg(double radians)
        {
            return radians * (180 / Math.PI);
        }

        /// <summary>
        /// Calculates the dot producto of two vectors
        /// </summary>
        /// <param name="v1">Vector 1</param>
        /// <param name="v2">Vector 2</param>
        /// <returns>The dot product</returns>
        public static double Dot(Vector3 v1, Vector3 v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }
        

        /// <summary>
        /// Calculates a point(in the form of 3D Vector) that is between two points and
        /// in a certain distance between the first point.
        /// </summary>
        /// <param name="src">The source point or 3D Vector where the distance is measured</param>
        /// <param name="dst">The destiny point where is calculated the line between</param>
        /// <param name="distance">The distance from the first point in mts</param>
        /// <returns>A 3D Vector situated between the two point with the given distance</returns>
        public static Vector3 PointBetweenDistance(Vector3 src, Vector3 dst, double distance)
        {
            double theta = CalculateDegrees(src, dst);
            theta = fitThetaPhase(src, dst, theta);
            return FindPointAtDistanceFrom(src, theta, distance / 1000);
        }

        /// <summary>
        /// Ajusta el angulo formado por dos puntos dependiendo del cuadrante en el que se encuentra
        /// el segundo punto
        /// </summary>
        /// <param name="src">Punto de origen</param>
        /// <param name="dst">Punto de destino</param>
        /// <param name="theta">Angulo formado entre los puntos</param>
        /// <returns></returns>
        private static double fitThetaPhase(Vector3 src, Vector3 dst, double theta)
        {
            if(src.X >= dst.X && src.Y >= dst.Y)
            {
                return theta + 180;
            }
            else if(src.X > dst.X && src.Y < dst.Y)
            {
                return 90 + (90 - theta);
            }
            else if(src.X <= dst.X && src.Y <= dst.Y)
            {
                return theta;
            }
            else if(src.X < dst.X && src.Y > dst.Y)
            {
                return 360 - theta;
            }
            return 0;
        }

        /// <summary>
        /// Calcula el angulo formado por la hipotenusa por dos puntos
        /// </summary>
        /// <param name="v1">vector 1</param>
        /// <param name="v2">vector 2</param>
        /// <param name="h">hipotenusa</param>
        /// <returns></returns>
        private static double CalculateDegrees(Vector3 v1, Vector3 v2)
        {
            var co = HaversineDistanceInMts(v1, new Vector3(v1.X, v2.Y, 0));
            var h = HaversineDistanceInMts(v1, v2);
            return radToDeg(Math.Asin(co / h));
        }


        /// <summary>
        /// Compare two vector if there are the same
        /// </summary>
        /// <param name="v">Vector to compare</param>
        /// <returns>True if the values are equal, false otherwise</returns>
        public bool Compare(Vector3 v)
        {
            if (X == v.X && Y == v.Y && Z == v.Z) return true;
            else return false;
        }
        
        /// <summary>
        /// Create a new instance of vector with the same values
        /// </summary>
        /// <returns>A vector with the same values of actual instance</returns>
        public Vector3 Clone()
        {
            return (Vector3)MemberwiseClone();
        }

        /// <summary>
        /// Calculates the percentual difference between two values
        /// </summary>
        /// <param name="v1">Value 1</param>
        /// <param name="v2">Value 2</param>
        /// <returns>Pecentual difference</returns>
        public static double PercentualDiff(double v1, double v2)
        {
            return Math.Abs((v1 - v2) / (v1 + v2 / 2)); 
        }

        public override string ToString()
        {
            return ("(" + _x.ToString() + ", " + _y.ToString() + ", " + _z.ToString() + ")").Replace(",", "."); 
        }
    }
}
