﻿using Newtonsoft.Json.Linq;
using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy
{
    static class Prices
    {
        private static double _gasoline_per_liter = 20;
        //private static double _rents = 0;
        private static double _rent_truck_month = 0;
        private static double _cost_truck = 0;
        //other...

        //or load from an external file
        static public void InitPrices()
        {
            _gasoline_per_liter = 20;
        }

        static public void LoadPrices()
        {
            if (File.Exists(@"c:\res\prices.json"))
            {
                JObject json = JObject.Parse(File.ReadAllText(@"c:\res\prices.json"));
                _gasoline_per_liter = json.Value<double>("gas per liter");
                _rent_truck_month = json.Value<double>("rent car per month");
                _cost_truck = json.Value<double>("cost of truck");
            }
            else
            {
                throw new System.IO.IOException("There is no file of prices in \res folder");
            } 
        }

        static public double GasolinePerLiter
        {
            get { return _gasoline_per_liter; }
            set { _gasoline_per_liter = value; }
        }

        static public double RentTruckMonth
        {
            get { return _rent_truck_month; }
            set { _rent_truck_month = value; }
        }
    }

}
